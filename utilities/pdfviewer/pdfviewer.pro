CONFIG         += object_parallel_to_source
QMAKE_CXXFLAGS += -std=c++17

MOC_DIR     = build_moc
OBJECTS_DIR = build_obj
QT         += core gui widgets webenginewidgets
CONFIG     += depend_includepath console

TARGET = pdfviewer
TEMPLATE = app

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.hpp

INCLUDEPATH *= $$PWD/../../

include($$PWD/../../qwgtlib.pri)
include($$PWD/../../../Algorithm/algorithm.pri)
include($$PWD/../../../DebugMacro/debug.pri)
include($$PWD/../../../Support/support.pri)

unix: PKGCONFIG += poppler-qt5
unix: CONFIG += link_pkgconfig
