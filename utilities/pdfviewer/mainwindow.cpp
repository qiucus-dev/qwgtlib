#include "mainwindow.hpp"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , pdfviewer(new wgtm::PdfViewer(this))
    , fileTree(new wgt::FileTreeViewer(this))
    , central(new QSplitter(this))
//  ,
{
    setCentralWidget(central);
    central->addWidget(fileTree);
    central->addWidget(pdfviewer);
    central->setChildrenCollapsible(false);
    central->setStretchFactor(1, 5);
    central->setOrientation(Qt::Horizontal);

    resize(1200, 900);

    fileTree->setRoot("/home/kamanji/Downloads/PDF");
    fileTree->addBannedExtensions({"pdf"});
    fileTree->setInvertBanned(true);

    connect(
        fileTree, &wgt::FileTreeViewer::fileSelected, [&](QString path) {
            QFileInfo info(path);
            LOG << "Opening file at" << path;
            if (info.suffix() == "pdf") {
                pdfviewer->openFile(path);
            }
        });
}
