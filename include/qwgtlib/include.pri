INCLUDEPATH *= $$PWD/..

HEADERS *= \
    $$PWD/*.hpp \
    $$PWD/base/*.hpp \
    $$PWD/textedit/*.hpp \
    $$PWD/utility/*.hpp \
    $$PWD/base/widgetmanageable.hpp \
    $$PWD/textedit/markdowneditor.hpp \
    $$PWD/pdfviewer.hpp \
    $$PWD/base/widgetinterface.hpp \
    $$PWD/webview.hpp
