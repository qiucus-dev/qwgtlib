TEMPLATE = lib
CONFIG += staticlib
TARGET = qwgtlib

QT += core gui widgets xml

include($$PWD/source/textedit/textedit.pri)
include($$PWD/source/base/base.pri)
include($$PWD/source/smartcontextmenu/smartcontextmenu.pri)
include($$PWD/source/keyboard/keyboard.pri)
include($$PWD/source/simple_widgets/simple_widgets.pri)
include($$PWD/source/utility/utility.pri)
include($$PWD/include/qwgtlib/include.pri)
