#include "radialbutton.hpp"
#include "smartcontextmenu_scene.hpp"

namespace wgt {
RadialButton::RadialButton(SmartContextMenu_Scene* _scene)
    : ContextMenuButton(_scene) {
}


void RadialButton::setPolarCoords(qreal radius, qreal deg) {
    radialDistance = std::fmod(radius, 360);
    radialDegree   = deg;
}

void RadialButton::setRadialLimit(qreal min, qreal max) {
    radialDegreeLimit.first  = min;
    radialDegreeLimit.second = max;
}

qreal RadialButton::getRadialStep() const {
    return (radialDegreeLimit.second - radialDegreeLimit.first)
           / buttonsOnLevel;
}


void RadialButton::updatePos() {
    radialDegree = radialDegreeLimit.first + getRadialStep() * buttonIndex;
    posX         = centerDx + cos(rad(radialDegree)) * radialDistance;
    posY         = centerDy - sin(rad(radialDegree)) * radialDistance;
    this->setPos(posX, posY);
}

uint RadialButton::getButtonIndex() const {
    return buttonIndex;
}

void RadialButton::setButtonIndex(const uint& value) {
    buttonIndex = value;
}

void RadialButton::setButtonsOnLevel(const uint& value) {
    buttonsOnLevel = value;
}


int RadialButton::getButtonDiam() const {
    return buttonDiam;
}


void RadialButton::fromJson(json& buttonSettings, json& globalSettings) {
    setActionName(QString::fromStdString(buttonSettings["action-name"]));
    setLevelIndex(buttonSettings["level-index"]);
    setButtonIndex(buttonSettings["button-index"]);

    try {
        json radialLimits = globalSettings["radial-limits"];
        setRadialLimit(
            radialLimits[levelIndex][0], radialLimits[levelIndex][1]);
    } catch (...) { setRadialLimit(0, 270); }

    try {
        QString imagePath = QString::fromStdString(
            buttonSettings["image"]);
        if (!imagePath.isEmpty()) {
            QString fullPath(scene->getSetingsFolder() + "/" + imagePath);
            image = QPixmap(fullPath);
        }
    } catch (...) {}

    try {
        QString imageHoverPath = QString::fromStdString(
            buttonSettings["image-hover"]);
        if (!imageHoverPath.isEmpty()) {
            QString fullPath(
                scene->getSetingsFolder() + "/" + imageHoverPath);
            imageHover = QPixmap(fullPath);
        }
    } catch (...) {}
}

QPixmap& RadialButton::getPainterImage() {
    if (hover && !imageHover.isNull()) {
        return imageHover;
    } else {
        return image;
    }
}

uint RadialButton::getLevelIndex() const {
    return levelIndex;
}

uint RadialButton::getLevelNumber() const {
    return levelIndex + 1;
}

void RadialButton::setLevelIndex(const uint& value) {
    levelIndex = value;
}

void RadialButton::setRadialDistance(const qreal& value) {
    radialDistance = value;
}

QRectF RadialButton::boundingRect() const {
    return QRectF(0, 0, buttonDiam, buttonDiam);
}

void RadialButton::paint(
    QPainter*                       painter,
    const QStyleOptionGraphicsItem* option,
    QWidget*                        widget) {
    Q_UNUSED(option)
    Q_UNUSED(widget)
    QPen   pen(Qt::red, 3);
    QRectF rect = boundingRect();
    painter->setPen(pen);
    if (!image.isNull()) {
        QPixmap& buttonImage = getPainterImage();
        QRectF imageRect(0, 0, buttonImage.width(), buttonImage.height());
        QRectF targetRect(0, 0, buttonDiam, buttonDiam);
        painter->drawPixmap(targetRect, buttonImage, imageRect);
    } else {
        painter->drawEllipse(rect);
    }
}
} // namespace wgt
