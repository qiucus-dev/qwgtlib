#include "regularmenubutton.hpp"


namespace wgt {
RegularMenuButton::RegularMenuButton(SmartContextMenu_Scene* _scene)
    : ContextMenuButton(_scene) {
}

qreal RegularMenuButton::getWidth() const {
    return buttonRect.width();
}

void RegularMenuButton::setWidth(const qreal& value) {
    buttonRect.setWidth(value);
}

QRectF RegularMenuButton::boundingRect() const {
    return buttonRect;
}

void RegularMenuButton::paint(
    QPainter*                       painter,
    const QStyleOptionGraphicsItem* option,
    QWidget*                        widget) {
    TEMPORARY_UNUSED(option);
    TEMPORARY_UNUSED(widget);
    QRectF rect = boundingRect();
    QBrush brush(Qt::white);
    painter->setBrush(brush);

    if (hover) {
        QPen pen(Qt::red, 3);
        painter->setPen(pen);
        painter->drawRect(rect);
    } else {
        QPen pen(Qt::black, 3);
        painter->setPen(pen);
        painter->drawRect(rect);
    }

    qreal textElevation = rect.height() / 2 + textHeight / 2;
    painter->drawText(textOffsetHorizontal, textElevation, menuName);
}

void RegularMenuButton::updatePos() {
    QPointF pos = {centerDx, centerDy};
    pos += QPointF(0, buttonIndex * buttonRect.height());
    this->setPos(pos);
}


void RegularMenuButton::fromJson(
    json& buttonSettings,
    json& globalSettings) {
    buttonRect.setHeight(globalSettings["button-height"]);
    //#    setActionName(buttonSettings["action-name"]);
    actionName  = QString::fromStdString(buttonSettings["action-name"]);
    buttonIndex = buttonSettings["button-index"];
    menuName    = QString::fromStdString(buttonSettings["menu-name"]);
    QFont        font;
    QFontMetrics metrics(font);
    textHeight = metrics.height();
    buttonRect.setWidth(
        2 * textOffsetHorizontal + metrics.width(menuName));
}
} // namespace wgt
