#ifndef RADIALBUTTON_HPP
#define RADIALBUTTON_HPP


#include "contextmenubutton.hpp"
#include "menuactionresult.hpp"
#include <utility>


namespace wgt {
class SmartContextMenu_Scene;

class RadialButton : public ContextMenuButton
{
  public:
    RadialButton(SmartContextMenu_Scene* _scene);

    void  setPolarCoords(qreal radius, qreal deg);
    void  setRadialLimit(qreal min, qreal max);
    qreal getRadialStep() const;
    void  posFromIndex(int buttonsNum, int _buttonIndex);
    void  setRadialDistance(const qreal& value);
    void  setLevelIndex(const uint& value);
    uint  getLevelIndex() const;
    uint  getLevelNumber() const;
    uint  getButtonIndex() const;
    void  setButtonIndex(const uint& value);
    void  setButtonsOnLevel(const uint& value);
    int   getButtonDiam() const;

    void updatePos() override;
    void fromJson(json& buttonSettings, json& globalSettings) override;

  private:
    int      buttonDiam     = 40;
    qreal    radialDistance = 0;
    qreal    radialDegree   = 0;
    uint     levelIndex     = 0;
    uint     buttonIndex    = 1;
    uint     buttonsOnLevel = 1;
    QPixmap  image;
    QPixmap  imageHover;
    QPixmap& getPainterImage();

    std::pair<qreal, qreal> radialDegreeLimit = {0, 270};


    // QGraphicsItem interface
  public:
    QRectF boundingRect() const override;
    void   paint(
          QPainter*                       painter,
          const QStyleOptionGraphicsItem* option,
          QWidget*                        widget) override;
};
} // namespace wgt

#endif
