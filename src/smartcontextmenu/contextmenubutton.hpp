#ifndef CONTEXTMENUBUTTON_HPP
#define CONTEXTMENUBUTTON_HPP

#include <QGraphicsItem>
#include <QPainter>
#include <QPen>
#include <QPixmap>
#include <QString>
#include <hsupport/qjson.hpp>
#include <hsupport/misc.hpp>

#include "scm_debug.hpp"

namespace wgt {
class SmartContextMenu_Scene;

class ContextMenuButton : public QGraphicsItem
{
  public:
    ContextMenuButton(SmartContextMenu_Scene* _scene);
    virtual void updatePos() = 0;
    QString      getActionName() const;
    void         setActionName(const QString& value);
    virtual void fromJson(json& buttonSettings, json& globalSettings) = 0;
    void         setDCenter(qreal dx, qreal dy);
    void         setCenterDeviation(qreal dx, qreal dy);

  protected:
    qreal   centerDx = 0;
    qreal   centerDy = 0;
    qreal   posX;
    qreal   posY;
    QString actionName;
    bool    hover = false;

    SmartContextMenu_Scene* scene = nullptr;

    // QGraphicsItem interface
  public:
    QRectF boundingRect() const override = 0;
    void   paint(
          QPainter*                       painter,
          const QStyleOptionGraphicsItem* option,
          QWidget*                        widget) override = 0;

    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;
};
} // namespace wgt

#endif // CONTEXTMENUBUTTON_HPP
