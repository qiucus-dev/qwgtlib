HEADERS *= \
    $$PWD/smartcontextmenu.hpp \
    $$PWD/smartcontextmenu_scene.hpp \
    $$PWD/menuactionresult.hpp \
    $$PWD/radialbutton.hpp \
    $$PWD/scm_debug.hpp \
    $$PWD/contextmenubutton.hpp \
    $$PWD/regularmenubutton.hpp

SOURCES *= \
    $$PWD/smartcontextmenu.cpp \
    $$PWD/smartcontextmenu_scene.cpp \
    $$PWD/radialbutton.cpp \
    $$PWD/contextmenubutton.cpp \
    $$PWD/regularmenubutton.cpp

DISTFILES += \
    $$PWD/context_menu.json

INCLUDEPATH *= $$PWD
