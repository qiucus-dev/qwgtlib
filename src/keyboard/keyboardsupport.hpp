#pragma once

#include <QEvent>
#include <QMap>
#include <QVector>
#include <hsupport/qjson.hpp>
#include <memory>

#include "kbdactionresolver.hpp"
#include "keyboardfilter.hpp"
#include "keyevent.hpp"
#include "keyseqence.hpp"
#include "shortcutresolver.hpp"

namespace wgt {
class KeyboardFilter;

/*!

\brief KeyboardSupport is an abstract interface class for keyboard
shortcuts handling

\todo Convert to abstract class

To implement keyboard support for widget you need to do the following

+ Derive from `KeyboardSupport` class
+ Implement `initShortcut()`, `keyPressEvent()` and `handleInput()`
  functions.

`processKeyEvent()` content:

``` c++
bool FileTree::processKeyEvent(KeyEvent& event) {
    QString       command = shortcuts.getCommand(event.getSequence());
    KBDParameters parameters;
    return executeCommand(command, parameters);
}
```

`keyPressEvent()` content

```
void FileTree::keyPressEvent(QKeyEvent* event) {
    KeyEvent keyEvent(event);
    processKeyEvent(keyEvent);
    QTreeView::keyPressEvent(event);

    // To show key pressed on debug window. Can be omitted
    DEBUG_FINALIZE_WINDOW
}
```

You need to override default shortcut handling for Qt widget buy
providing implementation for it. Code in the example should be enough
for the most cases: it is only used to pass keyboard signal to the
handlers.

`initShortcuts()` content example:

```
void FileTree::initShortcuts() {
    // Name of the key matched with command name
    shortcuts.addBinding("Backspace", "directory-up");
    // Command name matched with lambda that will be used
    // as callback when command is executed
    keyboardActions["directory-up"] =
        [&](KBDParameters& internalParameters,
            json&          userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)
        this->dirUp();
        return true;
    };
}
```



Each widget that derives from the `KeyboardSupport` has own copy of
shortcut settings in it (You can see `KeyboardSupport::shortcuts`
member variable). This allows to change shortcuts in per-widget basis
and avoid weird bugs that might be caused by lambda in `initShortcuts`
that have captured reference to wrong `this` widget.


\dotfile keyboard_support.dot "keyboard handling"

 */
class KeyboardSupport
{
  public:
    KeyboardSupport()          = default;
    virtual ~KeyboardSupport() = default;
    virtual bool    processKeyEvent(QEvent* event);
    virtual bool    processKeyEvent(KeyEvent& event);
    virtual QString getCommand(const KeySeqence& sequence);
    virtual bool    passDown(KeyEvent& event);
    virtual bool    runCommand(QString command);
    bool            isKeyboard(QEvent* event);
    QKeyEvent*      toKeyboard(QEvent* event);
    KeySeqence      toSequence(QEvent* event);

  protected:
    bool executeCommand(
        QString        command,
        KBDParameters& internalParameters,
        json&          userParameters);

    bool executeCommand(
        QString        command,
        KBDParameters& internalParameters);

    virtual void              updateChildren();
    ShortcutResolver          shortcuts;
    QVector<KeyboardSupport*> childWidgets;
    KBDActionResolver         keyboardActions;
    virtual void              initShortcuts();
    virtual void              initCommandActions();
};
} // namespace wgt
