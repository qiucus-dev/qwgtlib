#ifndef KBD_DEBUG_MACRO_HPP
#define KBD_DEBUG_MACRO_HPP

#include <hdebugmacro/all.hpp>

//#define KBDWIDGET_DEBUG

#ifdef KBDWIDGET_DEBUG
#    define KBD_LOG LOG
#    define KBD_WARN WARN
#    define KBD_INFO INFO
#    define KBD_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define KBD_FUNC_END DEBUG_FUNCTION_END
#    define KBD_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define KBD_LOG VOID_LOG
#    define KBD_WARN VOID_LOG
#    define KBD_INFO VOID_LOG
#    define KBD_FUNC_BEGIN
#    define KBD_FUNC_END
#    define KBD_FUNC_RET(message)
#endif

#endif // KBD_DEBUG_MACRO_HPP
