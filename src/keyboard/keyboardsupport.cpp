#include "keyboardsupport.hpp"
#include "kbd_debug_macro.hpp"

namespace wgt {
/// \brief Check if event can be processed on current level. If not pass on
/// lower level
/// \return True is processing was succesfull
bool KeyboardSupport::processKeyEvent(QEvent* event) {
    // TODO Provide default implementation: it is only slightly
    // different in all already implemented classed
    Q_UNUSED(event) return false;
}

/// \brief Check if event can be processed on current level. If not pass on
/// lower level
/// \return True is processing was succesfull
bool KeyboardSupport::processKeyEvent(KeyEvent& event) {
    Q_UNUSED(event) return false;
}

/// \return QString associated with this key sequence
QString KeyboardSupport::getCommand(const KeySeqence& sequence) {
    return shortcuts.getCommand(sequence);
    //#    return shortcuts->value(sequence, "");
}

/*!
 * \brief Pass event down to one of the child widgets
 * \return True if event has been processed succesfully
 */
bool KeyboardSupport::passDown(KeyEvent& event) {
    Q_UNUSED(event);
    return false;
}


/// \brief Run command using CommandHandler
/// \return True if command has been executed succesfully
bool KeyboardSupport::runCommand(QString command) {
    Q_UNUSED(command);
    KBD_LOG << __FUNCTION__;
    return false;
}

/// \return True if event is keyboard event
bool KeyboardSupport::isKeyboard(QEvent* event) {
    return event->type() == QEvent::KeyPress;
}

/// \return Result of static_cast<QKeyEvent*>(event)
QKeyEvent* KeyboardSupport::toKeyboard(QEvent* event) {
    return static_cast<QKeyEvent*>(event);
}

/// \return KeySequence object constructed from event
KeySeqence KeyboardSupport::toSequence(QEvent* event) {
    return KeySeqence(toKeyboard(event));
}

/// \brief Update list of child widgets
void KeyboardSupport::updateChildren() {
}

/// Used for initalization of all of the shortcuts
void KeyboardSupport::initShortcuts() {
}

// TODO Seems to be unused inside of the project
void KeyboardSupport::initCommandActions() {
}

// FIXME Why there is two implementations?
bool KeyboardSupport::executeCommand(
    QString        command,
    KBDParameters& internalParameters,
    json&          userParameters) {
    return keyboardActions[command](internalParameters, userParameters);
}

bool KeyboardSupport::executeCommand(
    QString        command,
    KBDParameters& internalParameters) {
    json userParameters;
    if (keyboardActions.hasCommand(command)) {
        KBD_LOG << "Executing" << command;
        return keyboardActions[command](
            internalParameters, userParameters);
    } else {
        return false;
    }
}
} // namespace wgt
