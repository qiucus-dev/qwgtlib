#ifndef KEYBOARDACTION_HPP
#define KEYBOARDACTION_HPP

#include <QString>
#include <hdebugmacro/all.hpp>
#include <hsupport/qjson.hpp>

#include <functional>

namespace wgt {
class KeyboardSupport;


class KBDParameters
{
  public:
    bool             atomic = true;
    KeyboardSupport* target = nullptr;
};

/// \brief Base class for all actions that can be called through
/// keyboard shortcuts
class KeyboardAction
{
  public:
    KeyboardAction();
    KeyboardAction(
        QString                                    _name,
        std::function<bool(KBDParameters&, json&)> _action);
    bool operator()(
        KBDParameters& internalParameter,
        json&          userParameters);
    KeyboardAction& operator=(
        std::function<bool(KBDParameters&, json&)> _action);

    QString getName() const;
    void    setName(const QString& value);

  private:
    QString                                    name;
    std::function<bool(KBDParameters&, json&)> action;
};
} // namespace wgt

#endif // KEYBOARDACTION_HPP
