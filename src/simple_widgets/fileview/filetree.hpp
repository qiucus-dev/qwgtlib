#ifndef FILETREE_H
#define FILETREE_H

#include "../../keyboard/keyboardsupport.hpp"
#include "../../smartcontextmenu/smartcontextmenu.hpp"
#include "filetreefilter.hpp"

#include <QAction>
#include <QClipboard>
#include <QDebug>
#include <QDir>
#include <QElapsedTimer>
#include <QFileSystemModel>
#include <QObject>
#include <QPoint>
#include <QTreeView>
#include <hdebugmacro/all.hpp>
#include <memory>
#include <utility>

namespace wgt {
/*!
 * \brief Simple file tree widget without any additional features
 */
class FileTree
    : public QTreeView
    , public KeyboardSupport
{
    Q_OBJECT
  public:
    explicit FileTree(QWidget* parent = nullptr);
    virtual ~FileTree() override = default;

    QString getFileFromIndex(const QModelIndex& index);

    //#=== File filtering
    void addBannedExtensions(std::vector<QString> files);
    bool isInvertedBanned() const;
    void setInvertBanned(bool value);


  public slots:
    void    dirUp();
    void    setRoot(QDir newRoot);
    void    setRoot(QString newRoot);
    QString getRoot() const;

  private:
    QDir              rootDirectory    = QDir("");
    QFileSystemModel* fileSystemModel  = nullptr;
    FileTreeFilter*   fileSystemFilter = nullptr;
    QFileInfo         contextMenuSettings;
    QString           getDuplicateFileName(
                  QString filePath,
                  QString append = "copy");

    QElapsedTimer             clickTimer;
    std::pair<qint64, qint64> clickedTwiceInterval = {500, 1500};

    void initUI();
    void initSignals();
    void renameFile(QModelIndex fileIndex);

  private slots:
    void showContextMenu(const QPoint& pos);
    void openIndex(const QModelIndex& index);
    void previewIndex(const QModelIndex& index);
    void smartContextMenuActionChosen(MenuActionResult result);
    void contextMenuEmptySpacePressed(
        QPoint       pressGlobalPos,
        QMouseEvent* event);

  signals:
    void fileSelected(QString absolutePath);
    void directorySelected(QString absolutePath);
    void rootChanged(QString absolutePath);
    void fileRenamed(QString oldPath, QString newPath);


    //#= QWidget interface
  protected:
    void mousePressEvent(QMouseEvent* event) override;

    //#= KeyboardSupport interface
  protected:
    void initShortcuts() override;
    void keyPressEvent(QKeyEvent* event) override;
    bool processKeyEvent(KeyEvent& event) override;
};
} // namespace wgt


#endif // FILETREE_H
