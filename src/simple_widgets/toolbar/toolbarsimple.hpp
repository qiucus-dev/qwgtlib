#ifndef TOOLBARSIMPLE_HPP
#define TOOLBARSIMPLE_HPP

#include "../../base/lodsupport.hpp"
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QWidget>
#include <halgorithm/all.hpp>
#include <algorithm>
#include <hdebugmacro/all.hpp>
#include <map>
#include <hsupport/misc.hpp>


/*!
 * \brief Simple toolbar with size adaptive (each widget can be assigned
 * importance and non-important widgets might be hidden/show depending on
 * available space (size of the widget)).
 */
class ToolbarSimple
    : public QWidget
    , public LODSupport
{
    Q_OBJECT
  public:
    explicit ToolbarSimple(QWidget* parent = nullptr);
    void addWidget(QWidget* widget, LODLevel importance = All);
    void addWidget(
        QWidget* widget,
        QSize    minSize,
        QSize    maxSize    = {-1, -1},
        LODLevel importance = All);

    void addSpacer(QSpacerItem* spacer);
    void removeWidget(QWidget* widget);
    void setSpacing(uint spacing);

  private:
    QHBoxLayout*                 layout;
    std::map<QWidget*, LODLevel> importanceMap;
    uint                         spacing = 2;

    // LODSupport interface
  public:
    void updateUIFromLOD() override;

  protected:
    QSize getTreshold(LODLevel lodLevel) override;

    // QWidget interface
  protected:
    void showEvent(QShowEvent* event) override;
};


#endif // TOOLBARSIMPLE_HPP
