#ifndef PDFVIEW_DERIVED_H
#define PDFVIEW_DERIVED_H

#include "../../base/widgetmanageable.hpp"

#include <QLabel>
#include <QListView>
#include <QObject>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QWidget>

#include "renderedpdf.hpp"
#include <halgorithm/qwidget_cptr.hpp>


namespace wgtm {
/*!
 * \brief Widget to preview pdf files
 */
class PdfViewer
    : extends QWidget
    , implements WidgetManageable
    , implements WidgetInterface
{
  public:
    explicit PdfViewer(QWidget* parent = nullptr);
    virtual ~PdfViewer() override;

  public slots:
    void openFile(QString path) override;

  private:
    spt::qwidget_cptr<QVBoxLayout> layout;
    spt::qwidget_cptr<QSpinBox>    pageSelect;
    spt::qwidget_cptr<QLabel>      imagePreview;
    std::unique_ptr<RenderedPDF>   document;
};


} // namespace wgtm

#endif // PDFVIEW_DERIVED_H
