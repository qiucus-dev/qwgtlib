#include "pdfviewer.hpp"
#include <QPushButton>

namespace wgtm {
//#  /////////////////////////////////////////////////////////////////////
//#  Special member funcitons
//#  /////////////////////////////////////////////////////////////////////

PdfViewer::PdfViewer(QWidget* parent)
    : QWidget(parent)
    , layout(new QVBoxLayout)
    , pageSelect(new QSpinBox(this))
    , imagePreview(new QLabel(this))
//  ,
{
    setLayout(layout);
    layout->setParent(this);
    imagePreview->setSizePolicy(
        QSizePolicy::Expanding, QSizePolicy::Expanding);

    layout->addWidget(imagePreview);
    layout->addWidget(pageSelect);

    layout->setContentsMargins(spt::zeroMargins());
    setContentsMargins(spt::zeroMargins());


    connect(
        pageSelect,
        QOverload<int>::of(&QSpinBox::valueChanged),
        [&](int newPage) {
            LOG << "Loaded page" << newPage;
            imagePreview->setPixmap(
                QPixmap::fromImage(document->getPage(newPage)));
        });
}


PdfViewer::~PdfViewer() {
    INFO << "Deleting widget manageanble";
    LOG << "Name" << getWidgetName();
}

void PdfViewer::openFile(QString path) {
    document.reset(new RenderedPDF);
    document->openFile(path);

    if (currentFile == path) {
        LOG << "The same document";
        imagePreview->setPixmap(
            QPixmap::fromImage(document->getPage(pageSelect->value())));
    } else {
        LOG << "Different document";
        currentFile = path;
        pageSelect->setRange(0, document->getPageCount() - 1);
        pageSelect->setValue(0);
        imagePreview->setPixmap(QPixmap::fromImage(document->getPage(0)));
    }
}


//#  /////////////////////////////////////////////////////////////////////
//#  Initialization functions
//#  /////////////////////////////////////////////////////////////////////


} // namespace wgtm
