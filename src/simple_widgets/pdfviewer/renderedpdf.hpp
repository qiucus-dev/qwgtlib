#ifndef RENDEREDPDF_HPP
#define RENDEREDPDF_HPP

#include <QImage>
#include <QPixmap>
#include <memory>
#include <poppler/qt5/poppler-qt5.h>
#include <unordered_map>
#include <vector>


class RenderedPDF
{
  public:
    void   openFile(QString path);
    QImage getPage(uint pageIdx, qreal dpiX = 72.0, qreal dpiY = 72.0);
    uint   getPageCount();

  private:
    std::unordered_map<uint, QImage>   rendered;
    std::unique_ptr<Poppler::Document> document;
};


#endif // RENDEREDPDF_HPP
