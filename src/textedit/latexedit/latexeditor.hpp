#ifndef LATEXEDITOR_HPP
#define LATEXEDITOR_HPP

#include <QHBoxLayout>
#include <QPushButton>
#include <QSplitter>
#include <QStackedWidget>
#include <QWidget>

#include <halgorithm/qwidget_cptr.hpp>


#include "../textedit.hpp"

class ToolbarSimple;

namespace wgt {
class LatexBuildPreview;
class LatexQuickPreview;
class LatexPreview;
class WaitingSpinner;
} // namespace wgt

namespace wgtm {
class CodeEditor;
}

namespace wgtm {
class LatexEditor
    : extends QWidget
    , implements TextEdit
    , implements WidgetInterface
{
    Q_OBJECT
  public:
    explicit LatexEditor(QWidget* parent = nullptr);

  private:
    spt::qwidget_cptr<wgt::LatexBuildPreview> buildPreview;
    spt::qwidget_cptr<wgt::LatexQuickPreview> quickPreview;
    spt::qwidget_cptr<QHBoxLayout>            layout;
    spt::qwidget_cptr<QSplitter>              splitter;
    spt::qwidget_cptr<wgtm::CodeEditor>       codeEditor;
    spt::qwidget_cptr<QVBoxLayout>            resultLayout;
    spt::qwidget_cptr<QPushButton>            previewSwitch;
    spt::qwidget_cptr<QWidget>                previewFrame;
    spt::qwidget_cptr<QStackedWidget>         resultPreview;
    spt::qwidget_cptr<ToolbarSimple>          previewToolbar;
    spt::qwidget_cptr<wgt::WaitingSpinner>    buildWait;

    void               setQuickPreview();
    void               setBuildPreview();
    bool               isInBuildMode;
    wgt::LatexPreview* getTexEditor();

    //#== WidgetInterface interface
  protected:
    void initUI() override;
    void initSignals() override;

    //#== TextEdit interface
  public:
    void setText(QString text) override;
};


} // namespace wgtm

#endif // LATEXEDITOR_HPP
