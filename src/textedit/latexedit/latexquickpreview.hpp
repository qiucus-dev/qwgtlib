#ifndef LATEXQUICKPREVIEW_HPP
#define LATEXQUICKPREVIEW_HPP

#include <QDir>
#include <QFile>
#include <QVBoxLayout>
#include <QWebEngineView>

#include <halgorithm/qwidget_cptr.hpp>

#include "latexpreview.hpp"

namespace wgt {
class LatexQuickPreview : public LatexPreview
{
    Q_OBJECT
  public:
    explicit LatexQuickPreview(QWidget* parent = nullptr);
    void setSource(QString source) override;
    void build() override;


  private:
    spt::qwidget_cptr<QWebEngineView> preview;
    QDir                              previewHeadTail;
};
} // namespace wgt

#endif // LATEXQUICKPREVIEW_HPP
