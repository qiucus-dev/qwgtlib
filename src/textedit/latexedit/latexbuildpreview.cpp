#include "latexbuildpreview.hpp"

#include <QDir>
#include <QTextStream>

#include <hdebugmacro/all.hpp>
#include <pdfviewer/pdfviewer.hpp>
#include <hsupport/misc.hpp>

namespace wgt {
LatexBuildPreview::LatexBuildPreview(QWidget* parent)
    : LatexPreview(parent)
    , pdfViewer(new wgtm::PdfViewer(this))
    , texBuild(new QProcess(this))
//  ,
{
    layout->addWidget(pdfViewer);

    connect(texBuild, &QProcess::started, [&]() {
        LOG << "Build started";
        this->buildStarted();
    });

    connect(
        texBuild,
        QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [&](int exitCode, QProcess::ExitStatus exitStatus) {
            Q_UNUSED(exitCode)
            Q_UNUSED(exitStatus)
            this->buildFinished();
            LOG << "Build finished";
        });
}

void LatexBuildPreview::setSource(QString source) {
    texSource = source;
}

void LatexBuildPreview::build() {
    DEBUG_FUNCTION_BEGIN_FANCY

    updatePath();
    createSourceFile();
    buildTex(sourceFile.absoluteFilePath());
    openPDF(resultFile.absoluteFilePath());

    DEBUG_FUNCTION_END_FANCY
}

void LatexBuildPreview::updateUI() {
    //    pdfViewer->updateUI();
}

void LatexBuildPreview::updatePath() {
    LOG << sourceFile.absoluteFilePath() << sourceFile.exists();
    LOG << resultFile.absoluteFilePath() << resultFile.exists();

    if (scratchBuild && !sourceFile.exists()) {
        QString fileName = spt::getRandomBase32(20);
        sourceFile.setFile(
            buildFolder.absoluteFilePath(fileName + ".tex"));
        resultFile.setFile(
            buildFolder.absoluteFilePath(fileName + ".pdf"));
        QDir().mkpath(sourceFile.absoluteDir().absolutePath());
        QFile(sourceFile.absoluteFilePath()).open(QIODevice::ReadOnly);
    }

    LOG << sourceFile.absoluteFilePath() << sourceFile.exists();
    LOG << resultFile.absoluteFilePath() << resultFile.exists();
}

QString LatexBuildPreview::getHead() {
    QFile file(headFile.absoluteFilePath());
    file.open(QIODevice::ReadOnly);
    return file.readAll();
}

QString LatexBuildPreview::getTail() {
    QFile file(tailFile.absoluteFilePath());
    file.open(QIODevice::ReadOnly);
    return file.readAll();
}

void LatexBuildPreview::buildTex(QString path) {
    if (texBuild->state() == QProcess::ProcessState::NotRunning) {
        QStringList args;
        QFileInfo   fileInfo(path);
        texBuild->setProgram("latexmk");
        texBuild->setArguments(
            {"-lualatex",
             "-interaction=nonstopmode",
             "-latexoption=--shell-escape",
             "-aux-directory=" + fileInfo.absolutePath(),
             "-output-directory=" + fileInfo.absolutePath(),
             path});

        LOG << texBuild->arguments().join("\t\n");

        texBuild->start();
    }
}

void LatexBuildPreview::openPDF(QString path) {
    pdfViewer->openFile(path);
}

void LatexBuildPreview::createSourceFile() {
    LOG << "Creating source file at path" << sourceFile.absoluteFilePath();
    QFile texFile(sourceFile.absoluteFilePath());
    texFile.open(QIODevice::WriteOnly);
    QTextStream stream(&texFile);
    stream << getHead() + texSource + getTail();
    texFile.close();
}

void LatexBuildPreview::setTailFile(const QFileInfo& value) {
    tailFile = value;
}


void LatexBuildPreview::setHeadFile(const QFileInfo& value) {
    headFile = value;
}


void LatexBuildPreview::setBuildFolder(const QDir& value) {
    buildFolder = value;
}
} // namespace wgt
