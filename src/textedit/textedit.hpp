#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include <QDebug>
#include <QObject>
#include <QPlainTextEdit>
#include <QTextEdit>
#include <QWidget>
#include <memory>

#include <hdebugmacro/all.hpp>


#include "../base/widgetmanageable.hpp"


namespace wgtm {
class TextEdit : public WidgetManageable
{
  public:
    TextEdit();
    virtual ~TextEdit();
    virtual QString getText();
    virtual void    setText(QString newText) = 0;
    virtual void    clean();
};
} // namespace wgtm

#endif // TEXTEDIT_H
