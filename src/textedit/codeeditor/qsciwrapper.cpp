#include "qsciwrapper.hpp"

namespace wgt::qsci {

//#  /////////////////////////////////////////////////////////////////////
//#  Special member funcitons
//#  /////////////////////////////////////////////////////////////////////

QSciWrapper::QSciWrapper(QWidget* parent) : QsciScintilla(parent) {
}

//#  /////////////////////////////////////////////////////////////////////
//#  Selection and positioning
//#  /////////////////////////////////////////////////////////////////////

lineIdx QSciWrapper::getCurrentPosLine() const {
    lineIdx line;
    colIdx  column;
    getCursorPosition(&line, &column);
    return line;
}


int QSciWrapper::getCurrentPosColumn() const {
    lineIdx line;
    colIdx  column;
    getCursorPosition(&line, &column);
    return column;
}


Pos QSciWrapper::getCurretPos() const {
    lineIdx line;
    colIdx  column;
    getCursorPosition(&line, &column);
    return Pos(line, column);
}

void QSciWrapper::setCurrentPos(Pos newPos) {
    setCursorPosition(newPos.line, newPos.col);
}

void QSciWrapper::setCurrentPos(int line, int column) {
    setCursorPosition(line, column);
}

CharRange QSciWrapper::getSelectionRange() const {
    lineIdx lineStart;
    colIdx  colStart;
    lineIdx lineEnd;
    colIdx  colEnd;

    getSelection(&lineStart, &colStart, &lineEnd, &colEnd);

    return CharRange({lineStart, colStart}, {lineEnd, colEnd});
}

void QSciWrapper::goToEOL() {
    setCurrentPos(getCurrentPosLine(), lineLength(getCurrentPosLine()));
}

void QSciWrapper::goToLineDown() {
    lineIdx currentLine   = getCurrentPosLine();
    colIdx  currentColumn = getCurrentPosColumn();
    if (isLastLine(currentLine)) {
        return;
    } else {
        setCurrentPos(currentLine + 1, currentColumn);
    }
}

//#  /////////////////////////////////////////////////////////////////////
//#  Text operations
//#  /////////////////////////////////////////////////////////////////////

void QSciWrapper::moveLineUp(lineIdx line) {
    DEBUG_SCI_WRAPPER_BEGIN

    Pos savedPos = getCurretPos();
    if (line == 0) {
        LOG << "Cannot move first line up";
        DEBUG_SCI_WRAPPER_END
        return;
    }
    LOG << "Moving line" << line;
    swapLines(line, line - 1);
    setCurrentPos(savedPos.line - 1, savedPos.col);


    DEBUG_SCI_WRAPPER_END
}

void QSciWrapper::moveLineDown(int line) {
    DEBUG_SCI_WRAPPER_BEGIN

    Pos savedPos  = getCurretPos();
    int lineCount = getLineCount();
    LOG << "Line count:" << lineCount;
    if (isLastLine(line)) {
        LOG << "Cannot move last line down";
        DEBUG_FUNCTION_END
        return;
    }
    LOG << "Moving line" << line;
    swapLines(line, line + 1);
    setCurrentPos(savedPos.line + 1, savedPos.col);


    DEBUG_SCI_WRAPPER_END
}

/*! \brief Swap first and second lines
 * \note Does not start/end new undo action
 * \param returnBack - bool. Wheter or not to return cursor to initial
 * position
 */
void QSciWrapper::swapLines(
    lineIdx source,
    lineIdx target,
    /// Add new line when attempting to switch last line down
    bool forceLastLineDown,
    /// Add new line when attemping to switch first line up
    bool forceFirstLineUp) {
    DEBUG_SCI_WRAPPER_BEGIN

    if (source > target) {
        std::swap(target, source);
    }

    QString sourceLine = getLineText(source);
    QString targetLine = getLineText(target);


    if (source >= getLineCount() - 2) {
        sourceLine.chop(1);
    }

    if (forceLastLineDown && isLastLine(target)) {
        sourceLine.append("\n");
    } else if (forceFirstLineUp && isFirstLine(target)) {
        sourceLine.append("\n");
    }

    setTargetRange(getLineRange(target));
    replaceTargetRange(sourceLine);

    setTargetRange(getLineRange(source));
    replaceTargetRange(targetLine);

    DEBUG_SCI_WRAPPER_END
}

void QSciWrapper::deleteLine(lineIdx line) {
    Pos currentPos = getCurretPos();
    setCursorPosition(line, 0);
    SendScintilla(SCI_LINEDELETE);
    SendScintilla(SCI_DELETEBACK);
    setCursorPosition(currentPos.line, currentPos.col);
}

void QSciWrapper::deleteCharLeft() {
    deleteRange(getCurretPos(), -1);
}

void QSciWrapper::deleteCharRight() {
    deleteRange(getCurretPos(), 1);
}

void QSciWrapper::setLineText(int line, QString& text) {
    LOG << "Setting line" << line << "to" << text;
    setTargetRange(getLineRange(line));
    replaceTargetRange(text);
}

/*!
 * \brief Return string between start and end of the range.
 * \note Changes target range
 */
QString QSciWrapper::getRangeText(CharRange range) {
    setTargetRange(range);
    return getTargetRangeText();
}

void QSciWrapper::replaceRange(CharRange range, QString& text) {
    setTargetRange(range);
    replaceTargetRange(text);
}

void QSciWrapper::deleteRange(docPos start, int offset) {
    SendScintilla(SCI_DELETERANGE, start, offset);
}

void QSciWrapper::deleteRange(Pos start, int offset) {
    SendScintilla(SCI_DELETERANGE, absPosFromPos(start), offset);
}

void QSciWrapper::setTargetRange(Pos pos, int offset) {
    docPos start = absPosFromPos(pos);
    docPos end   = start + offset;
    setTargetRange(start, end);
}

void QSciWrapper::setTargetRange(docPos start, docPos end) {
    SendScintilla(SCI_SETTARGETRANGE, start, end);
}

void QSciWrapper::setTargetRange(CharRange range) {
    docPos rangeStart = absPosFromPos(range.start);
    docPos rangeEnd   = absPosFromPos(range.end);
    SendScintilla(SCI_SETTARGETRANGE, rangeStart, rangeEnd);
}

QString QSciWrapper::getTargetRangeText() {
    std::vector<char> string(getTargetRangeSize());
    SendScintilla(SCI_GETTARGETTEXT, string.data());
    string.push_back('\0');
    LOG << "Getting target range text" << QString(string.data());
    return QString(string.data());
}

void QSciWrapper::replaceTargetRange(QString& text) {
    std::string string = text.toStdString();
    SendScintilla(SCI_REPLACETARGET, string.length(), string.c_str());
}

docPos QSciWrapper::getTargetRangeSize() {
    docPos start = SendScintilla(SCI_GETTARGETSTART);
    docPos end   = SendScintilla(SCI_GETTARGETEND);
    return start - end;
}


//#  /////////////////////////////////////////////////////////////////////
//#  Stats access
//#  /////////////////////////////////////////////////////////////////////


int QSciWrapper::getLineCount() {
    return SendScintilla(SCI_GETLINECOUNT);
}

int QSciWrapper::getLineLength(lineIdx line) const {
    return lineLength(line);
}


bool QSciWrapper::isLastLine(lineIdx line) {
    return (line + 1) == getLineCount();
}

bool QSciWrapper::isFirstLine(lineIdx line) const {
    return line == 0;
}


QString QSciWrapper::getLineText(lineIdx line) const {
    docPos lineStart = absLineStart(line);
    docPos lineEnd   = absLineEnd(line);
    char   lineText[lineEnd - lineStart + 2];

    SendScintilla(SCI_SETTARGETRANGE, lineStart, lineEnd);
    SendScintilla(SCI_GETTARGETTEXT, lineText);
    lineText[lineEnd - lineStart]     = '\n';
    lineText[lineEnd - lineStart + 1] = '\0';

    return QString(lineText);
}


docPos QSciWrapper::absPosFromPos(Pos pos) {
    docPos lineStart = SendScintilla(SCI_POSITIONFROMLINE, pos.line);
    docPos absPos    = lineStart + pos.col;
    return absPos;
}

Pos QSciWrapper::posFromAbsPos(docPos documentPos) {
    lineIdx line = SendScintilla(SCI_LINEFROMPOSITION, documentPos);
    long    lineStartAbs = SendScintilla(SCI_POSITIONFROMLINE, line);
    colIdx  col          = documentPos - lineStartAbs;
    return Pos(line, col);
}

docPos QSciWrapper::absLineStart(lineIdx line) {
    docPos res = SendScintilla(SCI_POSITIONFROMLINE, line);
    return res;
}

docPos QSciWrapper::absLineEnd(lineIdx line) {
    docPos res = SendScintilla(SCI_GETLINEENDPOSITION, line);
    return res;
}

CharRange QSciWrapper::getLineRange(int line) {
    return {{line, 0}, {line, lineLength(line)}};
}


void QSciWrapper::setSelection(CharRange range) {
    QsciScintilla::setSelection(
        range.start.line, //
        range.start.col,  //
        range.end.line,   //
        range.end.col);
}

void QSciWrapper::selectLine(int line) {
    setSelection(line, 0, line, lineLength(line));
}

void QSciWrapper::insertText(Pos pos, QString text) {
    docPos      absPos = absPosFromPos(pos);
    std::string stdStr = text.toStdString();
    SendScintilla(SCI_INSERTTEXT, absPos, stdStr.c_str());
}


void QSciWrapper::setSelection(
    int lineFrom,
    int colFrom,
    int lineTo,
    int colTo) {
    QsciScintilla::setSelection(lineFrom, colFrom, lineTo, colTo);
}

docPos QSciWrapper::absLineStart(lineIdx line) const {
    return SendScintilla(SCI_POSITIONFROMLINE, line);
}

docPos QSciWrapper::absLineEnd(lineIdx line) const {
    return SendScintilla(SCI_GETLINEENDPOSITION, line);
}


QString QSciWrapper::getDocumentText() const {
    return text();
}


//#  /////////////////////////////////////////////////////////////////////
//#  Comments
//#  /////////////////////////////////////////////////////////////////////

void QSciWrapper::setLineCommented(int line, spt::LexerType language) {
    CommentSettings settings = getCommentSettings(true, language);
    QString         lineText = getLineText(line);
    LOG << "Commenting line containing" << lineText;
    lineText = settings.openingString + lineText + settings.closingString;
    setLineText(line, lineText);
}

void QSciWrapper::setLineCommented(
    int                           line,
    QSciWrapper::CommentSettings& settings) {
    QString lineText = getLineText(line);
    lineText = settings.openingString + lineText + settings.closingString;
    setLineText(line, lineText);
}

void QSciWrapper::setRangeCommented(
    CharRange      range,
    spt::LexerType language) {
    CommentSettings settings = getCommentSettings(false, language);
    if (settings.eachLine) {
        for (int line : range.lines()) {
            setLineCommented(line, settings);
        }
    } else {
        insertText(range.start, settings.openingString);
        insertText(range.end, settings.closingString);
    }
}

QSciWrapper::CommentSettings QSciWrapper::getCommentSettings(
    int            singleLine,
    spt::LexerType lexer) {
    CommentSettings settings;
    if (singleLine) {
        switch (lexer) {
            case spt::LexerType::Bash: {
                settings.openingString = "#";
            } break;
            default: {};
        }
    } else {
        settings.multiLine = true;
        switch (lexer) {
            case spt::LexerType::Bash: {
                settings.openingString = "#";
                settings.eachLine      = true;
            } break;
            default: {};
        }
    }

    return settings;
}
} // namespace wgt::qsci
