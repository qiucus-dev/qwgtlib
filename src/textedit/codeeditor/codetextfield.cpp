#include "codeeditor.hpp"

// FIXME I'm 100% sure that all of the lexers are unnecessary. They
// slow down compilation time and generate unnecessary code bloat.
#include "qscilexerqss.h"
#include <Qsci/qscilexeravs.h>
#include <Qsci/qscilexerbash.h>
#include <Qsci/qscilexerbatch.h>
#include <Qsci/qscilexercmake.h>
#include <Qsci/qscilexercoffeescript.h>
#include <Qsci/qscilexercpp.h>
#include <Qsci/qscilexercsharp.h>
#include <Qsci/qscilexercss.h>
#include <Qsci/qscilexercustom.h>
#include <Qsci/qscilexerd.h>
#include <Qsci/qscilexerdiff.h>
#include <Qsci/qscilexeredifact.h>
#include <Qsci/qscilexerfortran.h>
#include <Qsci/qscilexerfortran77.h>
#include <Qsci/qscilexerhtml.h>
#include <Qsci/qscilexeridl.h>
#include <Qsci/qscilexerjava.h>
#include <Qsci/qscilexerjavascript.h>
#include <Qsci/qscilexerjson.h>
#include <Qsci/qscilexerlua.h>
#include <Qsci/qscilexermakefile.h>
#include <Qsci/qscilexermarkdown.h>
#include <Qsci/qscilexermatlab.h>
#include <Qsci/qscilexeroctave.h>
#include <Qsci/qscilexerpascal.h>
#include <Qsci/qscilexerperl.h>
#include <Qsci/qscilexerpo.h>
#include <Qsci/qscilexerpostscript.h>
#include <Qsci/qscilexerpov.h>
#include <Qsci/qscilexerproperties.h>
#include <Qsci/qscilexerpython.h>
#include <Qsci/qscilexerruby.h>
#include <Qsci/qscilexerspice.h>
#include <Qsci/qscilexersql.h>
#include <Qsci/qscilexertcl.h>
#include <Qsci/qscilexertex.h>
#include <Qsci/qscilexerverilog.h>
#include <Qsci/qscilexervhdl.h>
#include <Qsci/qscilexerxml.h>
#include <Qsci/qscilexeryaml.h>

#include <hdebugmacro/all.hpp>

namespace wgt {
CodeTextfield::CodeTextfield(QWidget* parent)
    : QSciWrapper(parent)
    , lexer(newLexer(spt::LexerType::Markdown))
//  ,
{
    setUtf8(true);

    setCaretLineVisible(true);
    setCaretLineBackgroundColor(QColor("gainsboro"));

    setAutoIndent(true);
    setIndentationGuides(false);
    setIndentationsUseTabs(false);
    setIndentationWidth(4);
    font.setPointSize(12);
    font.setFamily("Source Code Pro");

    lexer->setFont(font);
    setLexer(lexer.get());

    setMarginsBackgroundColor(QColor("gainsboro"));
    setMarginLineNumbers(1, true);
    setMarginWidth(1, 25);

    setBraceMatching(QsciScintilla::SloppyBraceMatch);
    setMatchedBraceBackgroundColor(Qt::yellow);
    setUnmatchedBraceForegroundColor(Qt::blue);

    initShortcuts();
}

void CodeTextfield::setNewLexer(spt::LexerType type) {
    lexerType = type;
    lexer.reset(newLexer(type));
    lexer->setFont(font);
    setLexer(lexer.get());
}

QsciLexer* CodeTextfield::newLexer(spt::LexerType type) const {
    // clang-format off
    switch (type) {
        case spt::LexerType::Avs          : return new QsciLexerAVS          ;
        case spt::LexerType::Bash         : return new QsciLexerBash         ;
        case spt::LexerType::Batch        : return new QsciLexerBatch        ;
        case spt::LexerType::Cmake        : return new QsciLexerCMake        ;
        case spt::LexerType::Coffeescript : return new QsciLexerCoffeeScript ;
        case spt::LexerType::Cpp          : return new QsciLexerCPP          ;
        case spt::LexerType::Csharp       : return new QsciLexerCSharp       ;
        case spt::LexerType::Css          : return new QsciLexerCSS          ;
        case spt::LexerType::D            : return new QsciLexerD            ;
        case spt::LexerType::Diff         : return new QsciLexerDiff         ;
        case spt::LexerType::Edifact      : return new QsciLexerEDIFACT      ;
        case spt::LexerType::Fortran      : return new QsciLexerFortran      ;
        case spt::LexerType::Fortran77    : return new QsciLexerFortran77    ;
        case spt::LexerType::Html         : return new QsciLexerHTML         ;
        case spt::LexerType::Idl          : return new QsciLexerIDL          ;
        case spt::LexerType::Java         : return new QsciLexerJava         ;
        case spt::LexerType::Javascript   : return new QsciLexerJavaScript   ;
        case spt::LexerType::Json         : return new QsciLexerJSON         ;
        case spt::LexerType::Lua          : return new QsciLexerLua          ;
        case spt::LexerType::Makefile     : return new QsciLexerMakefile     ;
        case spt::LexerType::Markdown     : return new QsciLexerMarkdown     ;
        case spt::LexerType::Matlab       : return new QsciLexerMatlab       ;
        case spt::LexerType::Octave       : return new QsciLexerOctave       ;
        case spt::LexerType::Pascal       : return new QsciLexerPascal       ;
        case spt::LexerType::Perl         : return new QsciLexerPerl         ;
        case spt::LexerType::Po           : return new QsciLexerPO           ;
        case spt::LexerType::Postscript   : return new QsciLexerPostScript   ;
        case spt::LexerType::Pov          : return new QsciLexerPOV          ;
        case spt::LexerType::Properties   : return new QsciLexerProperties   ;
        case spt::LexerType::Python       : return new QsciLexerPython       ;
        case spt::LexerType::Ruby         : return new QsciLexerRuby         ;
        case spt::LexerType::Spice        : return new QsciLexerSpice        ;
        case spt::LexerType::Sql          : return new QsciLexerSQL          ;
        case spt::LexerType::Tcl          : return new QsciLexerTCL          ;
        case spt::LexerType::Tex          : return new QsciLexerTeX          ;
        case spt::LexerType::Verilog      : return new QsciLexerVerilog      ;
        case spt::LexerType::Vhdl         : return new QsciLexerVHDL         ;
        case spt::LexerType::Xml          : return new QsciLexerXML          ;
        case spt::LexerType::Yaml         : return new QsciLexerYAML         ;
        case spt::LexerType::Custom       : return new QsciLexerMarkdown     ;
        case spt::LexerType::Qss          : return new QsciLexerQSS          ;
    }
    // clang-format on
}

void CodeTextfield::keyPressEvent(QKeyEvent* e) {
    KBD_FUNC_BEGIN

    if ((e->modifiers() == Qt::CTRL) && (e->key() == Qt::Key_Space)) {
        autoCompleteFromAll();
        return;
    }

    KeyEvent event(e);

    KBD_LOG << "CodeTextfield key press event"
            << event.getSequence().toString();

    if (!processKeyEvent(event)) {
        KBD_WARN << "Unknown shortcut";
        KBD_LOG << "Passing event to parent";
        this->parent()->event(e);
        //        QsciScintilla::keyPressEvent(e);
    } else {
        KBD_LOG << "Command executed correctly";
    }

    KBD_FUNC_END
    DEBUG_FINALIZE_WINDOW
}

bool CodeTextfield::processKeyEvent(KeyEvent& event) {
    KBD_FUNC_BEGIN

    QString       command = shortcuts.getCommand(event.getSequence());
    KBDParameters parameters;

    KBD_FUNC_END
    return executeCommand(command, parameters);
}

void CodeTextfield::initShortcuts() {
    shortcuts.addBinding("alt+up", "move-current-line-up");
    keyboardActions["move-current-line-up"] =
        [&](KBDParameters& internalParameters,
            json&          userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)
        moveLineUp(getCurrentPosLine());
        return true;
    };

    shortcuts.addBinding("alt+down", "move-current-line-down");
    keyboardActions["move-current-line-down"] =
        [&](KBDParameters& internalParameters,
            json&          userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)
        moveLineDown(getCurrentPosLine());
        return true;
    };

    shortcuts.addBinding("shift+del", "delete-current-line");
    keyboardActions["delete-current-line"] =
        [&](KBDParameters& internalParameters,
            json&          userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)
        deleteLine(getCurrentPosLine());
        return true;
    };

    shortcuts.addBinding("ctrl+b", "build-requested");
    keyboardActions["build-requested"] =
        [&](KBDParameters& internalParameters,
            json&          userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)
        this->buildRequested();
        return true;
    };


    shortcuts.addBinding("ctrl+/", "comment-selection-out");
    keyboardActions["comment-selection-out"] =
        [&](KBDParameters& internalParameters,
            json&          userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)
        KBD_LOG << "Comment requested";

        if (hasSelectedText()) {
            setRangeCommented(getSelectionRange(), lexerType);
        } else {
            setLineCommented(getCurrentPosLine(), lexerType);
        }

        return true;
    };

    //    shortcuts.addBinding("ctrl+s", "emit-save-requested");
    //    keyboardActions["emit-save-requested"] =
    //        [&](wgt::KBDParameters& internalParameters,
    //            json&               userParameters) -> bool {
    //        Q_UNUSED(userParameters)
    //        Q_UNUSED(internalParameters)
    //        KBD_LOG << BACK_GREEN("CodeTextfield emit-save-requested");
    //        this->saveRequested();
    //        return true;
    //    };
}
} // namespace wgt
