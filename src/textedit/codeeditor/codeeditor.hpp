#ifndef CODEEDITOR_H
#define CODEEDITOR_H


/*!
 * \file codeeditor.hpp
 * \brief
 */

//===    Qt    ===//
#include <QDebug>
#include <QFile>
#include <QPainter>
#include <QPlainTextEdit>
#include <QTextBlock>
#include <QTextStream>
#include <QVBoxLayout>
#include <QWidget>


//===    STL   ===//
#include <cassert>
#include <memory>


//=== Sibling  ===//
#include "../../base/widgetmanageable.hpp"
#include "../../keyboard/keyboardfilter.hpp"
#include "../../keyboard/keyboardsupport.hpp"
#include "../../keyboard/keyevent.hpp"
#include "../../keyboard/keyseqence.hpp"
#include "../../textedit/textedit.hpp"
#include "codetextfield.hpp"


//#  ////////////////////////////////////


namespace wgtm {
class CodeEditor
    : public QWidget
    , public TextEdit
{
    Q_OBJECT


  public:
    explicit CodeEditor(QWidget* parent = nullptr);
    void setLexer(spt::LexerType type);
    virtual ~CodeEditor() override;

  private:
    wgt::CodeTextfield* textField;
    QVBoxLayout*        mainLayout;
    void                setTextWrap(bool isWrapped = true);


  signals:
    void buildRequested();
    void saveRequested();

    // WidgetManageable interface
  public:
    QString getWidgetName() const override;

  public slots:
    void openFile(QString path) override;

  protected:
    void writeFile(QFile& file) override;

    // TextEdit interface
  public:
    QString getText() override;
    void    setText(QString newText) override;
};


} // namespace wgtm
#endif // CODEEDITOR_H
