#include "codeeditor.hpp"

#include <hdebugmacro/all.hpp>

namespace wgtm {
CodeEditor::CodeEditor(QWidget* parent)
    : QWidget(parent)
    , textField(new wgt::CodeTextfield(this))
    , mainLayout(new QVBoxLayout) {
    mainLayout->addWidget(textField);
    this->setLayout(mainLayout);
    this->setContentsMargins(spt::zeroMargins());
    mainLayout->setContentsMargins(spt::zeroMargins());
    setTextWrap(true);

    connect(textField, &wgt::CodeTextfield::buildRequested, [&]() {
        INFO << "Code editor textfield build requested";
        this->buildRequested();
    });

    connect(textField, &wgt::CodeTextfield::saveRequested, [&]() {
        this->saveRequested();
    });
}

void CodeEditor::setLexer(spt::LexerType type) {
    textField->setNewLexer(type);
}

CodeEditor::~CodeEditor() {
    INFO << "Deleting widget manageanble";
    LOG << "Name" << this->getWidgetName();
}

void CodeEditor::setTextWrap(bool isWrapped) {
    if (isWrapped) {
        textField->setWrapMode(QsciScintilla::WrapWord);
    } else {
        textField->setWrapMode(QsciScintilla::WrapNone);
    }
}

QString CodeEditor::getWidgetName() const {
    return "CodeEditor";
}

QString CodeEditor::getText() {
    return textField->getDocumentText();
}

void CodeEditor::setText(QString newText) {
    textField->setText(newText);
}

} // namespace wgtm
