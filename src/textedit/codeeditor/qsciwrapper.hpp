#ifndef QSCISCINTILLAWRAPPER_HPP
#define QSCISCINTILLAWRAPPER_HPP

#include <Qsci/qsciscintilla.h>
#include <hdebugmacro/all.hpp>
#include <utility>

#define DEBUG_SCI_WRAPPER_BEGIN DEBUG_FUNCTION_BEGIN
#define DEBUG_SCI_WRAPPER_END DEBUG_FUNCTION_END

namespace spt {
enum class LexerType
{
    Avs,
    Bash,
    Batch,
    Cmake,
    Coffeescript,
    Cpp,
    Csharp,
    Css,
    Custom,
    D,
    Diff,
    Edifact,
    Fortran,
    Fortran77,
    Html,
    Idl,
    Java,
    Javascript,
    Json,
    Lua,
    Makefile,
    Markdown,
    Matlab,
    Octave,
    Pascal,
    Perl,
    Po,
    Postscript,
    Pov,
    Properties,
    Python,
    Ruby,
    Spice,
    Sql,
    Tcl,
    Tex,
    Verilog,
    Vhdl,
    Xml,
    Yaml,
    Qss,
};
}

namespace wgt {
/*!
 * \brief QScintilla-related widgets, algorithms and structs
 */
namespace qsci {
    typedef long docPos;
    typedef int  lineIdx;
    typedef int  colIdx;

    struct Pos {
        Pos() {
        }
        Pos(docPos _line, docPos _col) : line(_line), col(_col) {
        }
        lineIdx line;
        colIdx  col;
        bool    operator<(const Pos& other) {
            return line < other.line || col < other.col;
        }

        bool operator>(const Pos& other) {
            return line > other.line || col > other.col;
        }
    };

    struct CharRange {
        Pos              start;
        Pos              end;
        std::vector<int> lines() {
            std::vector<int> res(end.line - start.line);
            for (int i = start.line; i <= end.line; ++i) {
                res.push_back(i);
            }
            return res;
        }
        CharRange() {
        }
        CharRange(Pos _start, Pos _end) {
            start = _start;
            end   = _end;
        }
    };

    class QSciWrapper : public QsciScintilla
    {
      public:
        QSciWrapper(QWidget* parent = nullptr);

        //#======    Selection and positioning
        CharRange getSelectionRange() const;
        lineIdx   getCurrentPosLine() const;
        int       getCurrentPosColumn() const;
        Pos       getCurretPos() const;
        void      setCurrentPos(Pos newPos);
        void      setCurrentPos(int line, int column);
        void      goToEOL();
        void      goToLineDown();
        void      selectLine(int line);
        void      setSelection(CharRange range);
        docPos    absPosFromPos(Pos pos);
        Pos       posFromAbsPos(docPos documentPos);
        docPos    absLineStart(lineIdx line);
        docPos    absLineEnd(lineIdx line);
        CharRange getLineRange(int line);
        void      setSelection(
                 int lineFrom, //
                 int colFrom,  //
                 int lineTo,   //
                 int colTo) override;

        //#======    Text operations

        void moveLineUp(lineIdx line);
        void moveLineDown(int line);
        void swapLines(
            lineIdx source,
            lineIdx target,
            bool    forceLastLineDown = false,
            bool    forceFirstLineUp  = false);
        void    deleteLine(lineIdx line);
        void    deleteCharLeft();
        void    deleteCharRight();
        QString getLineText(lineIdx line) const;
        void    setLineText(int line, QString& text);
        QString getRangeText(CharRange range);
        void    insertText(Pos pos, QString text);
        QString getDocumentText() const;
        void    replaceRange(CharRange range, QString& text);
        void    deleteRange(docPos start, int offset);
        void    deleteRange(Pos start, int offset);

      private:
        void    setTargetRange(Pos pos, int offset);
        void    setTargetRange(docPos start, docPos end);
        void    setTargetRange(CharRange range);
        QString getTargetRangeText();
        void    replaceTargetRange(QString& text);
        docPos  getTargetRangeSize();

        //#======    Stats access
      public:
        int    getLineCount();
        int    getLineLength(lineIdx line) const;
        bool   isLastLine(lineIdx line);
        bool   isFirstLine(lineIdx line) const;
        docPos absLineStart(lineIdx line) const;
        docPos absLineEnd(lineIdx line) const;

        //#======    Comments

        void setLineCommented(int line, spt::LexerType language);
        void setRangeCommented(CharRange range, spt::LexerType language);

      private:
        struct CommentSettings {
            bool multiLine = false;
            bool eachLine = false; ///< Languages that do not have explicit
                                   ///< multiline comments
            QString openingString = "//";
            QString closingString = "";
        };

        CommentSettings getCommentSettings(
            int            singleLine,
            spt::LexerType lexer);
        void setLineCommented(int line, CommentSettings& settings);
    };
} // namespace qsci
} // namespace wgt

#endif // QSCISCINTILLAWRAPPER_HPP
