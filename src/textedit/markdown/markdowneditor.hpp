#ifndef MARKDOWNVIEW_H
#define MARKDOWNVIEW_H

#include "../../base/widgetmanageable.hpp"
#include "../codeeditor.hpp"
#include "../textedit.hpp"
#include "markdowndocument.hpp"
#include "previewpage.hpp"


#include <QByteArray>
#include <QDir>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QSplitter>
#include <QWebChannel>
#include <QWebEngineView>
#include <QWidget>

namespace wgtm {
class MarkdownEditor
    : extends QWidget
    , implements TextEdit
{
    Q_OBJECT
  public:
    explicit MarkdownEditor(QWidget* parent = nullptr);

  private:
    QHBoxLayout*      mainLayout;
    QWebChannel*      webChannel;
    wgt::PreviewPage* previewPage;
    CodeEditor*       editor;
    QWebEngineView*   preview;
    QSplitter*        splitter;
    MarkdownDocument  document;

  protected:
    void writeFile(QString path) override;

    // WidgetManageable interface
  public:
    bool    isSupported(const QString& extension) override;
    QString getExtension() const override;

  public slots:
    void newFile() override;
    void openFile(QString path) override;

    //#== TextEdit interface
  public:
    void setText(QString text) override;
};
} // namespace wgtm

#endif // MARKDOWNVIEW_H
