#include "markdowndocument.hpp"

void MarkdownDocument::setText(const QString& textNew) {
    if (text == textNew) {
        return;
    }
    text = textNew;
    emit textChanged(text);
}
