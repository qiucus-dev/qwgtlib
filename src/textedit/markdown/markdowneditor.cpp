#include "markdowneditor.hpp"

namespace wgtm {
MarkdownEditor::MarkdownEditor(QWidget* parent)
    : QWidget(parent)
    , mainLayout(new QHBoxLayout)
    , webChannel(new QWebChannel(this))
    , previewPage(new wgt::PreviewPage(this))
    , editor(new CodeEditor(this))
    , preview(new QWebEngineView(this))
    , splitter(new QSplitter(this))

{
    editor->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
    preview->setContextMenuPolicy(Qt::NoContextMenu);

    preview->setPage(previewPage);

    webChannel->registerObject(QStringLiteral("content"), &document);
    previewPage->setWebChannel(webChannel);
    preview->setUrl(QUrl("qrc:/index.html"));

    splitter->insertWidget(0, editor);
    splitter->insertWidget(1, preview);
    splitter->setSizes(QList<int>({INT_MAX, INT_MAX}));

    mainLayout->addWidget(splitter);

    this->setLayout(mainLayout);

    this->supportedExtensions.clear();
    this->supportedExtensions.push_back("md");
}

bool MarkdownEditor::isSupported(const QString& extension) {
    return extension == "md";
}

void MarkdownEditor::openFile(QString path) {
    QFile file(path);
    file.open(QIODevice::ReadWrite);
    editor->setText(file.readAll());
    currentFile = path;
}

void MarkdownEditor::newFile() {
    this->currentFile = "";
    this->editor->setText("");
}

void MarkdownEditor::writeFile(QString path) {
    QFile out(path);
    if (!out.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return;
    }

    QTextStream outStr(&out);
    outStr << editor->getText();
}

QString MarkdownEditor::getExtension() const {
    return "md";
}

void wgtm::MarkdownEditor::setText(QString text) {
    editor->setText(text);
}


} // namespace wgtm
