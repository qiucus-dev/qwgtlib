#include "adoceditor.hpp"

namespace wgtm {
ADOCEditor::ADOCEditor(QWidget* parent)
    : QWidget(parent)
    , mainLayout(new QVBoxLayout)
    , textEdit(new CodeEditor(this)) {
    setLayout(mainLayout);
    mainLayout->addWidget(textEdit);
}

void ADOCEditor::setText(QString newText) {
    textEdit->setText(newText);
}
} // namespace wgtm
