#include "richtexteditor.hpp"


namespace wgtm {
QString RichTextEditor::getText() {
    return textfield->toHtml();
}

void RichTextEditor::clean() {
    textfield->clear();
}

QString RichTextEditor::toPlainText() const {
    return textfield->toPlainText();
}

QTextDocument* RichTextEditor::document() {
    return textfield->document();
}


QTextCursor RichTextEditor::textCursor() const {
    return textfield->textCursor();
}


void RichTextEditor::setTextCursor(const QTextCursor& cursor) {
    textfield->setTextCursor(cursor);
}


void RichTextEditor::setPlainText(const QString& text) {
    textfield->setPlainText(text);
}


void RichTextEditor::setHtml(const QString& text) {
    textfield->setHtml(text);
}


void RichTextEditor::setText(QString text) {
    RTE_FUNC_BEGIN

    if (text.isEmpty()) {
        setPlainText(text);
    } else if (Qt::mightBeRichText(text)) {
        setHtml(text);
    } else {
        setPlainText(text);
    }

    RTE_FUNC_END
}
} // namespace wgtm
