#include "rte_textfield.hpp"


namespace wgtm {
RTE_Textfield::RTE_Textfield(QWidget* parent) : QTextEdit(parent) {
    setContextMenuPolicy(Qt::CustomContextMenu);
    initSignals();
    initShortcuts();
}

void RTE_Textfield::initSignals() {
    connect(
        this,
        &RTE_Textfield::customContextMenuRequested,
        this,
        &RTE_Textfield::showContextMenu);
}

void RTE_Textfield::setContextMenuSettingsPath(const QFileInfo& value) {
    contextMenuSettingsPath = value;
}
} // namespace wgtm
