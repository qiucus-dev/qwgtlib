#ifndef RICHTEXTEDITOR_H
#define RICHTEXTEDITOR_H


/*!
 * \file rte_textfield.hpp
 */

//===    Qt    ===//
#include <QBuffer>
#include <QByteArray>
#include <QColorDialog>
#include <QImage>
#include <QMimeData>
#include <QPoint>
#include <QTextCursor>
#include <QTextDocument>
#include <QTextEdit>


//=== Sibling  ===//
#include "../../base/widgetinterface.hpp"
#include "../../keyboard/keyboardsupport.hpp"
#include "../../smartcontextmenu/menuactionresult.hpp"
#include "../../smartcontextmenu/smartcontextmenu.hpp"
#include "rte_debug_macro.hpp"
#include "rte_support.hpp"


//  ////////////////////////////////////


namespace wgtm {
class RTE_Textfield
    : extends QTextEdit
    , implements WidgetInterface
    , implements wgt::KeyboardSupport
{
    Q_OBJECT
  public:
    RTE_Textfield(QWidget* parent = nullptr);

    //# Text editing

    void dropImage(const QImage& image, const QString& format);

  protected:
    bool canInsertFromMimeData(const QMimeData* source) const override;
    void insertFromMimeData(const QMimeData* source) override;
    QMimeData* createMimeDataFromSelection() const override;

    void smartContextMenuActionChosen(wgt::MenuActionResult result);
    void contextMenuEmptySpacePressed(
        QPoint       pressGlobalPos,
        QMouseEvent* event);

    void initSignals() override;

  public slots:
    void setTextBold(bool cycle = false);
    void setTextItalic(bool cycle = false);
    void setTextUnderlined(bool cycle = false);
    void setTextStrikethrough(bool cycle = false);
    void insertTable(int rows, int columns, QTextTableFormat tableFormat);
    void setTableCellColor(QColor color);
    void insertHline();

  private slots:
    void showContextMenu(const QPoint& pos);

    //# Member access
  public:
    void setContextMenuSettingsPath(const QFileInfo& value);

  private:
    QFileInfo contextMenuSettingsPath;

    //#= QWidget interface
  protected:
    void mousePressEvent(QMouseEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;

    //#= KeyboardSupport interface
  public:
    bool processKeyEvent(wgt::KeyEvent& event) override;

  protected:
    void initShortcuts() override;
};
} // namespace wgtm


#endif // RICHTEXTEDITOR_H
