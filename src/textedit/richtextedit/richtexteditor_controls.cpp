#include "richtexteditor.hpp"

namespace wgtm {
void RichTextEditor::textSource() {
    QDialog*        dialog = new QDialog(this);
    QPlainTextEdit* pte    = new QPlainTextEdit(dialog);
    pte->setPlainText(textfield->toHtml());
    QGridLayout* gl = new QGridLayout(dialog);
    gl->addWidget(pte, 0, 0, 1, 1);
    dialog->setWindowTitle(tr("Document source"));
    dialog->setMinimumWidth(400);
    dialog->setMinimumHeight(600);
    dialog->exec();

    textfield->setHtml(pte->toPlainText());

    delete dialog;
}

void RichTextEditor::initSignals() {
    connect(
        textfield,
        &RTE_Textfield::currentCharFormatChanged,
        this,
        &RichTextEditor::slotCurrentCharFormatChanged);

    connect(
        textfield,
        &RTE_Textfield::cursorPositionChanged,
        this,
        &RichTextEditor::slotCursorPositionChanged);

    connect(
        insertHline_tbtn,
        &QToolButton::clicked,
        textfield,
        &RTE_Textfield::insertHline);

    connect(
        fontSize_cbox,
        SIGNAL(activated(QString)),
        this,
        SLOT(textSize(QString)));

    connect(
        foregColor_tbtn,
        &QToolButton::clicked,
        this,
        &RichTextEditor::textFgColor);

    connect(
        backgColor_tbtn,
        &QToolButton::clicked,
        this,
        &RichTextEditor::textBgColor);

    connect(
        insertImage_tbtn,
        &QToolButton::clicked,
        this,
        &RichTextEditor::insertImage);

    connect(
        this,
        &RichTextEditor::tableInsertionRequested,
        textfield,
        &RTE_Textfield::insertTable);
}

void RichTextEditor::textRemoveFormat() {
    QTextCharFormat fmt;
    fmt.setFontWeight(QFont::Normal);
    fmt.setFontUnderline(false);
    fmt.setFontStrikeOut(false);
    fmt.setFontItalic(false);
    fmt.setFontPointSize(9);

    bold_tbtn->setChecked(false);
    underline_tbtn->setChecked(false);
    italic_tbtn->setChecked(false);
    strikeout_tbtn->setChecked(false);
    fontSize_cbox->setCurrentIndex(
        fontSize_cbox->findText(defaultFontSize));

    fmt.clearBackground();

    mergeFormatOnWordOrSelection(fmt);
}

void RichTextEditor::textRemoveAllFormat() {
    bold_tbtn->setChecked(false);
    underline_tbtn->setChecked(false);
    italic_tbtn->setChecked(false);
    strikeout_tbtn->setChecked(false);
    fontSize_cbox->setCurrentIndex(
        fontSize_cbox->findText(defaultFontSize));
    QString text = textfield->toPlainText();
    textfield->setPlainText(text);
}

void RichTextEditor::textBold() {
    QTextCharFormat fmt;
    fmt.setFontWeight(
        bold_tbtn->isChecked() ? QFont::Bold : QFont::Normal);
    mergeFormatOnWordOrSelection(fmt);
}

void RichTextEditor::focusInEvent(QFocusEvent*) {
    textfield->setFocus(Qt::TabFocusReason);
}

void RichTextEditor::initMenuButtons() {
    // paragraph formatting
    {
        m_paragraphItems << tr("Standard") << tr("Heading 1")
                         << tr("Heading 2") << tr("Heading 3")
                         << tr("Heading 4") << tr("Monospace");
        paragraph_cbox->addItems(m_paragraphItems);

        connect(
            paragraph_cbox,
            SIGNAL(activated(int)),
            this,
            SLOT(textStyle(int)));
    }

    // undo & redo
    {
        // undoTBtn->setShortcut(QKeySequence::Undo);
        // redoTBtn->setShortcut(QKeySequence::Redo);

        connect(
            textfield->document(),
            SIGNAL(undoAvailable(bool)),
            undo_tbtn,
            SLOT(setEnabled(bool)));
        connect(
            textfield->document(),
            SIGNAL(redoAvailable(bool)),
            redo_tbtn,
            SLOT(setEnabled(bool)));

        undo_tbtn->setEnabled(textfield->document()->isUndoAvailable());
        redo_tbtn->setEnabled(textfield->document()->isRedoAvailable());

        connect(undo_tbtn, SIGNAL(clicked()), textfield, SLOT(undo()));
        connect(redo_tbtn, SIGNAL(clicked()), textfield, SLOT(redo()));
    }

    // cut, copy & paste
    {
        // cutTBtn->setShortcut(QKeySequence::Cut);
        // copyTBtn->setShortcut(QKeySequence::Copy);
        // pasteTBtn->setShortcut(QKeySequence::Paste);

        cut_tbtn->setEnabled(false);
        copy_tbtn->setEnabled(false);

        connect(cut_tbtn, SIGNAL(clicked()), textfield, SLOT(cut()));
        connect(copy_tbtn, SIGNAL(clicked()), textfield, SLOT(copy()));
        connect(paste_tbtn, SIGNAL(clicked()), textfield, SLOT(paste()));

        connect(
            textfield,
            SIGNAL(copyAvailable(bool)),
            cut_tbtn,
            SLOT(setEnabled(bool)));
        connect(
            textfield,
            SIGNAL(copyAvailable(bool)),
            copy_tbtn,
            SLOT(setEnabled(bool)));

#ifndef QT_NO_CLIPBOARD
        connect(
            QApplication::clipboard(),
            SIGNAL(dataChanged()),
            this,
            SLOT(slotClipboardDataChanged()));
#endif
    }

    // link
    {
        // linkTBtn->setShortcut(Qt::CTRL + Qt::Key_L);

        connect(
            link_tbtn, SIGNAL(clicked(bool)), this, SLOT(textLink(bool)));
    }

    // bold, italic & underline
    {
        // boldTBtn->setShortcut(Qt::CTRL + Qt::Key_B);
        // italicTBtn->setShortcut(Qt::CTRL + Qt::Key_I);
        // underlineTBtn->setShortcut(Qt::CTRL + Qt::Key_U);

        connect(bold_tbtn, SIGNAL(clicked()), this, SLOT(textBold()));
        connect(italic_tbtn, SIGNAL(clicked()), this, SLOT(textItalic()));
        connect(
            underline_tbtn,
            SIGNAL(clicked()),
            this,
            SLOT(textUnderline()));
        connect(
            strikeout_tbtn,
            SIGNAL(clicked()),
            this,
            SLOT(textStrikeout()));

        QAction* removeFormat = new QAction(
            tr("Remove character formatting"), this);
        // removeFormat->setShortcut(QKeySequence("CTRL+M"));
        connect(
            removeFormat,
            SIGNAL(triggered()),
            this,
            SLOT(textRemoveFormat()));
        textfield->addAction(removeFormat);

        QAction* removeAllFormat = new QAction(
            tr("Remove all formatting"), this);
        connect(
            removeAllFormat,
            SIGNAL(triggered()),
            this,
            SLOT(textRemoveAllFormat()));
        textfield->addAction(removeAllFormat);

        QAction* textsource = new QAction(
            tr("Edit document source"), this);
        // textsource->setShortcut(QKeySequence("CTRL+O"));
        connect(textsource, SIGNAL(triggered()), this, SLOT(textSource()));
        textfield->addAction(textsource);

        QMenu* menu = new QMenu(this);
        menu->addAction(removeAllFormat);
        menu->addAction(removeFormat);
        menu->addAction(textsource);
        menu_tbtn->setMenu(menu);
        menu_tbtn->setPopupMode(QToolButton::InstantPopup);
    }

    // lists
    {
        // bulletListTBtn->setShortcut(Qt::CTRL + Qt::Key_Minus);
        // numberedList_tbtn->setShortcut(Qt::CTRL + Qt::Key_Equal);

        connect(
            bulletList_tbtn,
            SIGNAL(clicked(bool)),
            this,
            SLOT(listBullet(bool)));
        connect(
            numberedList_tbtn,
            SIGNAL(clicked(bool)),
            this,
            SLOT(listOrdered(bool)));
    }

    // indentation
    {
        // indentLess_tbtn->setShortcut(Qt::CTRL + Qt::Key_Comma);
        // indentMore_tbtn->setShortcut(Qt::CTRL + Qt::Key_Period);

        connect(
            indentMore_tbtn,
            SIGNAL(clicked()),
            this,
            SLOT(increaseIndentation()));
        connect(
            indentLess_tbtn,
            SIGNAL(clicked()),
            this,
            SLOT(decreaseIndentation()));
    }
}

void RichTextEditor::textUnderline() {
    QTextCharFormat fmt;
    fmt.setFontUnderline(underline_tbtn->isChecked());
    mergeFormatOnWordOrSelection(fmt);
}

void RichTextEditor::textItalic() {
    QTextCharFormat fmt;
    fmt.setFontItalic(italic_tbtn->isChecked());
    mergeFormatOnWordOrSelection(fmt);
}

void RichTextEditor::textStrikeout() {
    QTextCharFormat fmt;
    fmt.setFontStrikeOut(strikeout_tbtn->isChecked());
    mergeFormatOnWordOrSelection(fmt);
}

void RichTextEditor::textSize(const QString& p) {
    qreal pointSize = p.toFloat();
    if (p.toFloat() > 0) {
        QTextCharFormat fmt;
        fmt.setFontPointSize(pointSize);
        mergeFormatOnWordOrSelection(fmt);
    }
}

void RichTextEditor::textLink(bool checked) {
    bool            unlink = false;
    QTextCharFormat fmt;
    if (checked) {
        QString url = textfield->currentCharFormat().anchorHref();
        bool    ok;
        QString newUrl = QInputDialog::getText(
            this,
            tr("Create a link"),
            tr("Link URL:"),
            QLineEdit::Normal,
            url,
            &ok);
        if (ok) {
            fmt.setAnchor(true);
            fmt.setAnchorHref(newUrl);
            fmt.setForeground(
                QApplication::palette().color(QPalette::Link));
            fmt.setFontUnderline(true);
        } else {
            unlink = true;
        }
    } else {
        unlink = true;
    }
    if (unlink) {
        fmt.setAnchor(false);
        fmt.setForeground(QApplication::palette().color(QPalette::Text));
        fmt.setFontUnderline(false);
    }
    mergeFormatOnWordOrSelection(fmt);
}

void RichTextEditor::textStyle(int index) {
    QTextCursor cursor = textfield->textCursor();
    cursor.beginEditBlock();

    // standard
    if (!cursor.hasSelection()) {
        cursor.select(QTextCursor::BlockUnderCursor);
    }
    QTextCharFormat fmt;
    cursor.setCharFormat(fmt);
    textfield->setCurrentCharFormat(fmt);

    if (index == ParagraphHeading1 || index == ParagraphHeading2
        || index == ParagraphHeading3 || index == ParagraphHeading4) {
        if (index == ParagraphHeading1) {
            fmt.setFontPointSize(fontsize_h1);
        }
        if (index == ParagraphHeading2) {
            fmt.setFontPointSize(fontsize_h2);
        }
        if (index == ParagraphHeading3) {
            fmt.setFontPointSize(fontsize_h3);
        }
        if (index == ParagraphHeading4) {
            fmt.setFontPointSize(fontsize_h4);
        }
        if (index == ParagraphHeading2 || index == ParagraphHeading4) {
            fmt.setFontItalic(true);
        }

        fmt.setFontWeight(QFont::Bold);
    }
    if (index == ParagraphMonospace) {
        fmt = cursor.charFormat();
        fmt.setFontFamily("Monospace");
        fmt.setFontStyleHint(QFont::Monospace);
        fmt.setFontFixedPitch(true);
    }
    cursor.setCharFormat(fmt);
    textfield->setCurrentCharFormat(fmt);

    cursor.endEditBlock();
}

void RichTextEditor::textFgColor() {
    QColor      col = QColorDialog::getColor(textfield->textColor(), this);
    QTextCursor cursor = textfield->textCursor();
    if (!cursor.hasSelection()) {
        cursor.select(QTextCursor::WordUnderCursor);
    }
    QTextCharFormat fmt = cursor.charFormat();
    if (col.isValid()) {
        fmt.setForeground(col);
    } else {
        fmt.clearForeground();
    }
    cursor.setCharFormat(fmt);
    textfield->setCurrentCharFormat(fmt);
    fgColorChanged(col);
}

void RichTextEditor::textBgColor() {
    QColor col = QColorDialog::getColor(
        textfield->textBackgroundColor(), this);
    QTextCursor cursor = textfield->textCursor();
    if (!cursor.hasSelection()) {
        cursor.select(QTextCursor::WordUnderCursor);
    }
    QTextCharFormat fmt = cursor.charFormat();
    if (col.isValid()) {
        fmt.setBackground(col);
    } else {
        fmt.clearBackground();
    }
    cursor.setCharFormat(fmt);
    textfield->setCurrentCharFormat(fmt);
    bgColorChanged(col);
}

void RichTextEditor::listBullet(bool checked) {
    if (checked) {
        numberedList_tbtn->setChecked(false);
    }
    list(checked, QTextListFormat::ListDisc);
}

void RichTextEditor::listOrdered(bool checked) {
    if (checked) {
        bulletList_tbtn->setChecked(false);
    }
    list(checked, QTextListFormat::ListDecimal);
}

void RichTextEditor::list(bool checked, QTextListFormat::Style style) {
    QTextCursor cursor = textfield->textCursor();
    cursor.beginEditBlock();
    if (!checked) {
        QTextBlockFormat obfmt = cursor.blockFormat();
        QTextBlockFormat bfmt;
        bfmt.setIndent(obfmt.indent());
        cursor.setBlockFormat(bfmt);
    } else {
        QTextListFormat listFmt;
        if (cursor.currentList()) {
            listFmt = cursor.currentList()->format();
        }
        listFmt.setStyle(style);
        cursor.createList(listFmt);
    }
    cursor.endEditBlock();
}

void RichTextEditor::mergeFormatOnWordOrSelection(
    const QTextCharFormat& format) {
    QTextCursor cursor = textfield->textCursor();
    if (!cursor.hasSelection()) {
        cursor.select(QTextCursor::WordUnderCursor);
    }
    cursor.mergeCharFormat(format);
    textfield->mergeCurrentCharFormat(format);
    textfield->setFocus(Qt::TabFocusReason);
}

void RichTextEditor::slotCursorPositionChanged() {
    QTextList* l = textfield->textCursor().currentList();
    if (m_lastBlockList
        && (l == m_lastBlockList
            || (l != 0 && m_lastBlockList != 0
                && l->format().style()
                       == m_lastBlockList->format().style()))) {
        return;
    }
    m_lastBlockList = l;
    if (l) {
        QTextListFormat lfmt = l->format();
        if (lfmt.style() == QTextListFormat::ListDisc) {
            bulletList_tbtn->setChecked(true);
            numberedList_tbtn->setChecked(false);
        } else if (lfmt.style() == QTextListFormat::ListDecimal) {
            bulletList_tbtn->setChecked(false);
            numberedList_tbtn->setChecked(true);
        } else {
            bulletList_tbtn->setChecked(false);
            numberedList_tbtn->setChecked(false);
        }
    } else {
        bulletList_tbtn->setChecked(false);
        numberedList_tbtn->setChecked(false);
    }
}

void RichTextEditor::fontChanged(const QFont& f) {
    fontSize_cbox->setCurrentIndex(
        fontSize_cbox->findText(QString::number(f.pointSize())));
    bold_tbtn->setChecked(f.bold());
    italic_tbtn->setChecked(f.italic());
    underline_tbtn->setChecked(f.underline());
    strikeout_tbtn->setChecked(f.strikeOut());
    if (f.pointSize() == fontsize_h1) {
        paragraph_cbox->setCurrentIndex(ParagraphHeading1);
    } else if (f.pointSize() == fontsize_h2) {
        paragraph_cbox->setCurrentIndex(ParagraphHeading2);
    } else if (f.pointSize() == fontsize_h3) {
        paragraph_cbox->setCurrentIndex(ParagraphHeading3);
    } else if (f.pointSize() == fontsize_h4) {
        paragraph_cbox->setCurrentIndex(ParagraphHeading4);
    } else {
        if (f.fixedPitch() && f.family() == "Monospace") {
            paragraph_cbox->setCurrentIndex(ParagraphMonospace);
        } else {
            paragraph_cbox->setCurrentIndex(ParagraphStandard);
        }
    }
    if (textfield->textCursor().currentList()) {
        QTextListFormat lfmt = textfield->textCursor()
                                   .currentList()
                                   ->format();
        if (lfmt.style() == QTextListFormat::ListDisc) {
            bulletList_tbtn->setChecked(true);
            numberedList_tbtn->setChecked(false);
        } else if (lfmt.style() == QTextListFormat::ListDecimal) {
            bulletList_tbtn->setChecked(false);
            numberedList_tbtn->setChecked(true);
        } else {
            bulletList_tbtn->setChecked(false);
            numberedList_tbtn->setChecked(false);
        }
    } else {
        bulletList_tbtn->setChecked(false);
        numberedList_tbtn->setChecked(false);
    }
}

void RichTextEditor::fgColorChanged(const QColor& c) {
    QPixmap pix(16, 16);
    if (c.isValid()) {
        pix.fill(c);
    } else {
        pix.fill(QApplication::palette().foreground().color());
    }
    foregColor_tbtn->setIcon(pix);
}

void RichTextEditor::bgColorChanged(const QColor& c) {
    QPixmap pix(16, 16);
    if (c.isValid()) {
        pix.fill(c);
    } else {
        pix.fill(QApplication::palette().background().color());
    }
    backgColor_tbtn->setIcon(pix);
}

void RichTextEditor::slotCurrentCharFormatChanged(
    const QTextCharFormat& format) {
    fontChanged(format.font());
    bgColorChanged(
        (format.background().isOpaque()) ? format.background().color()
                                         : QColor());
    fgColorChanged(
        (format.foreground().isOpaque()) ? format.foreground().color()
                                         : QColor());
    link_tbtn->setChecked(format.isAnchor());
}

void RichTextEditor::slotClipboardDataChanged() {
#ifndef QT_NO_CLIPBOARD
    if (const QMimeData* md = QApplication::clipboard()->mimeData())
        paste_tbtn->setEnabled(md->hasText());
#endif
}

QString RichTextEditor::toHtml() const {
    QString s = textfield->toHtml();
    // convert emails to links
    s = s.replace(
        QRegExp("(<[^a][^>]+>(?:<span[^>]+>)?|\\s)([a-zA-Z\\d]+@[a-"
                "zA-Z\\d]+\\.[a-zA-Z]+)"),
        "\\1<a href=\"mailto:\\2\">\\2</a>");
    // convert links
    s = s.replace(
        QRegExp("(<[^a][^>]+>(?:<span[^>]+>)?|\\s)((?:https?|ftp|"
                "file)://[^\\s'\"<>]+)"),
        "\\1<a href=\"\\2\">\\2</a>");
    // see also: Utils::linkify()
    return s;
}

void RichTextEditor::increaseIndentation() {
    indent(+1);
}

void RichTextEditor::decreaseIndentation() {
    indent(-1);
}

void RichTextEditor::indent(int delta) {
    QTextCursor cursor = textfield->textCursor();
    cursor.beginEditBlock();
    QTextBlockFormat bfmt = cursor.blockFormat();
    int              ind  = bfmt.indent();
    if (ind + delta >= 0) {
        bfmt.setIndent(ind + delta);
    }
    cursor.setBlockFormat(bfmt);
    cursor.endEditBlock();
}


void RichTextEditor::insertImage() {
    QSettings s;
    QString   attdir = s.value("general/filedialog-path").toString();
    QString   file   = QFileDialog::getOpenFileName(
        this,
        tr("Select an image"),
        attdir,
        tr("JPEG (*.jpg);; GIF (*.gif);; PNG "
           "(*.png);; BMP (*.bmp);; All (*)"));
    QImage image = QImageReader(file).read();

    textfield->dropImage(
        image, QFileInfo(file).suffix().toUpper().toLocal8Bit().data());
}


bool RichTextEditor::isSupported(const QString& extension) {
    return extension == "html";
}


QString RichTextEditor::getWidgetName() const {
    return "RichTextWidget";
}


void RichTextEditor::newFile() {
    if (!this->toPlainText().isEmpty()) {
        this->saveFile();
    }
    this->setHtml("");
}


void RichTextEditor::writeFile(QString path) {
    QFile file(path);
    file.open(QIODevice::ReadWrite);
    qDebug() << "Rich text widget write file";
    QTextStream out(&file);
    out << this->toHtml();
    file.flush();
    file.close();
}


void RichTextEditor::openFile(QString path) {
    QFile file(path);
    if (file.exists()) {
        file.open(QIODevice::ReadWrite);
        QByteArray  fileData = file.readAll();
        QTextCodec* codec    = Qt::codecForHtml(fileData);
        QString     str      = codec->toUnicode(fileData);
        if (Qt::mightBeRichText(str)) {
            this->textfield->setHtml(str);
        } else {
            str = QString::fromLocal8Bit(fileData);
            this->textfield->setPlainText(str);
        }

        currentFile = path;
    } else {
        qDebug() << "File does not exist";
        return;
    }
}

} // namespace wgtm
