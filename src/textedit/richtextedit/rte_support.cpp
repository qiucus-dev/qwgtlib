#include "rte_support.hpp"

namespace spt {
void setTableCellColor(QColor color, QTextCursor cursor) {
    QTextTable* table = cursor.currentTable();
    if (table == nullptr) {
        return;
    } else {
        QTextCharFormat format = table->cellAt(cursor).format();
        QBrush          brush(color);
        format.setBackground(brush);
        table->cellAt(cursor).setFormat(format);
    }
}
} // namespace spt
