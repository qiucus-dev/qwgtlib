#ifndef RICHTEXTWIDGET_UI_H
#define RICHTEXTWIDGET_UI_H

#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QDebug>
#include <QDir>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QSpacerItem>
#include <QToolButton>
#include <QVBoxLayout>
#include <QVariant>
#include <QWidget>

#include "../../utility/uiseparator.hpp"

#include "../../base/lodsupport.hpp"
#include "../../base/widgetinterface.hpp"
#include "../../simple_widgets/toolbar/toolbarsimple.hpp"
#include "inserttabledialog.hpp"
#include "rte_textfield.hpp"

namespace wgtm {
class RTE_Widget
    : extends QWidget
    , implements LODSupport
    , implements WidgetInterface
{
    Q_OBJECT
  public:
    explicit RTE_Widget(QString _iconDir, QWidget* parent = nullptr);

  protected:
    QComboBox*   paragraph_cbox;
    QToolButton* undo_tbtn;
    QToolButton* redo_tbtn;
    QToolButton* cut_tbtn;
    QToolButton* copy_tbtn;
    QToolButton* paste_tbtn;
    QToolButton* link_tbtn;
    QToolButton* bold_tbtn;
    QToolButton* italic_tbtn;
    QToolButton* underline_tbtn;
    QToolButton* strikeout_tbtn;
    QToolButton* bulletList_tbtn;
    QToolButton* numberedList_tbtn;
    QToolButton* indentLess_tbtn;
    QToolButton* indentMore_tbtn;
    QToolButton* foregColor_tbtn;
    QToolButton* backgColor_tbtn;
    QToolButton* insertHline_tbtn;
    QToolButton* menu_tbtn;
    QToolButton* insertImage_tbtn;
    QToolButton* insertTable_tbtn;
    QComboBox*   fontSize_cbox;

    RTE_Textfield* textfield;

  private:
    QVBoxLayout*   mainLayout;
    ToolbarSimple* toolbar;

    void setupButtons();
    void setupUI();
    void setupSignals();
    void showTableDialog(QPoint pos);

    uint  buttonHeight;
    QSize iconSize;
    QSize buttonMinSize;

  signals:
    void tableInsertionRequested(
        int              rows,
        int              columns,
        QTextTableFormat tableFormat);

    //#= QWidget interface
  protected:
    void resizeEvent(QResizeEvent* event) override;
};
} // namespace wgtm

Q_DECLARE_METATYPE(QTextTableFormat)
#endif // RICHTEXTWIDGET_UI_H
