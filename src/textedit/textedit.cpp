#include "textedit.hpp"

namespace wgtm {
TextEdit::TextEdit() {
}

TextEdit::~TextEdit() {
}

QString TextEdit::getText() {
    DEBUG_BASE_CLASS_FUNCTION_WARNING
    return "Text edit base text";
}

void TextEdit::setText(QString newText) {
    DEBUG_BASE_CLASS_FUNCTION_WARNING
    BASE_CLASS_UNUSED(newText)
}

void TextEdit::clean() {
    DEBUG_BASE_CLASS_FUNCTION_WARNING
}


} // namespace wgtm
