#include "widgetinterface.hpp"

void WidgetInterface::initUI() {
}

void WidgetInterface::initMenu() {
}

void WidgetInterface::initControls() {
}

void WidgetInterface::initContent() {
}

void WidgetInterface::initActions() {
}

void WidgetInterface::initSignals() {
}

void WidgetInterface::updateUI() {
}

QDir WidgetInterface::getIconDir() const {
    return iconDir;
}


/*!
 * Set directory that contains icons that will be used in UI
 */
void WidgetInterface::setIconDir(const QDir& value) {
    iconDir = value;
}

/*!
 * Set directory that contains icons that will be used in UI
 */
void WidgetInterface::setIconDir(QString absPath) {
    QFileInfo info(absPath);
    iconDir = info.absoluteFilePath();
}
