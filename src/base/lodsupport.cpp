#include "lodsupport.hpp"
#include <hdebugmacro/all.hpp>


LODSupport::LODSupport() {
}

LODSupport::~LODSupport() {
}

void LODSupport::setLOD(LODSupport::LODLevel newLOD) {
    currentLOD = newLOD;
}

void LODSupport::setUIDensity(LODSupport::UIDensity newUIDensity) {
    currentDensity = newUIDensity;
}

void LODSupport::updateUIFromLOD() {
}

void LODSupport::fitToSize(QSize widgetSize) {
    LOD_LOG << "Fit to size";
    LOD_LOG << "Size :" << widgetSize;
    LODLevel newLOD = lodThreshold.getLODLevel(widgetSize);
    if (newLOD == currentLOD) {
        return;
    } else {
        currentLOD = newLOD;
        updateUIFromLOD();
    }
}

void LODSupport::updateToNewLOD(LODSupport::LODLevel newLOD) {
    BASE_CLASS_UNUSED(newLOD);
}

void LODSupport::updateToNewDensity(LODSupport::UIDensity newDensity) {
    BASE_CLASS_UNUSED(newDensity);
}

bool LODSupport::lodFits(LODSupport::LODLevel source, LODLevel target) {
    return target >= source;
}

void LODSupport::decreaseLOD() {
    switch (currentLOD) {
        case All: currentLOD = Medium; break;
        case Medium: currentLOD = Minimal; break;
        case Minimal: currentLOD = Zero; break;
        case Zero: currentLOD = Zero; break;
    }
}

void LODSupport::increaseLOD() {
    switch (currentLOD) {
        case All: currentLOD = All; break;
        case Medium: currentLOD = All; break;
        case Minimal: currentLOD = Medium; break;
        case Zero: currentLOD = Minimal; break;
    }
}

void LODSupport::increaseDensity() {
    switch (currentDensity) {
        case Cramped: currentDensity = Cramped; break;
        case Compact: currentDensity = Cramped; break;
        case Maximized: currentDensity = Compact; break;
    }
}

void LODSupport::decreaseDensity() {
    switch (currentDensity) {
        case Cramped: currentDensity = Compact; break;
        case Compact: currentDensity = Maximized; break;
        case Maximized: currentDensity = Maximized; break;
    }
}

void LODSupport::set_cramped_minimal() {
}

void LODSupport::set_cramped_all() {
}

void LODSupport::set_maximized_all() {
}

void LODSupport::clear_all_layouts() {
}

void LODSupport::updateTresholds() {
    LOD_FUNC_BEGIN

    lodThreshold.ALL     = getTreshold(LODLevel::All);
    lodThreshold.Medium  = getTreshold(LODLevel::Medium);
    lodThreshold.Minimal = getTreshold(LODLevel::Minimal);
    lodThreshold.Zero    = getTreshold(LODLevel::Zero);

    LOD_INFO << "Treshold for ALL     :" << lodThreshold.ALL;
    LOD_INFO << "Treshold for Medium  :" << lodThreshold.Medium;
    LOD_INFO << "Treshold for Minimal :" << lodThreshold.Minimal;
    LOD_INFO << "Treshold for Zero    :" << lodThreshold.Zero;


    LOD_FUNC_END
}


QSize LODSupport::getTreshold(LODSupport::LODLevel lodLevel) {
    switch (lodLevel) {
        case All: return lodThreshold.ALL;
        case Medium: return lodThreshold.Medium;
        case Minimal: return lodThreshold.Minimal;
        case Zero: return lodThreshold.Zero;
    }
}

LODSupport::LODThreshold LODSupport::getLodThreshold() const {
    return lodThreshold;
}

void LODSupport::setLodThreshold(const LODThreshold& value) {
    lodThreshold = value;
}


/// \brief Get LOD level for treshold that can fit target
LODSupport::LODLevel LODSupport::LODThreshold::getLODLevel(QSize target) {
    LOD_FUNC_BEGIN

    LOD_LOG << "Target size" << target;

    if (sizeFits(ALL, target)) {
        LOD_FUNC_RET("LODLevel::All")
        return LODLevel::All;
    } else if (sizeFits(Medium, target)) {
        LOD_FUNC_RET("LODLevel::Medium")
        return LODLevel::Medium;
    } else if (sizeFits(Minimal, target)) {
        LOD_FUNC_RET("LODLevel::Minimal")
        return LODLevel::Minimal;
    } else {
        LOD_FUNC_RET("LODLevel::Zero")
        return LODLevel::Zero;
    }
}


/// \brief Check if source size is smaller or equal to target
/// I.e height and width of source is smaller or equal to target
bool LODSupport::LODThreshold::sizeFits(QSize source, QSize target) {
    return (
        source.height() <= target.height()
        && source.width() <= target.width());
}
