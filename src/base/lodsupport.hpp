#ifndef LOGSUPPORT_HPP
#define LOGSUPPORT_HPP

#include "lod_debug_macro.hpp"
#include <QSize>
#include <QVariant>

class LODSupport
{
  public:
    LODSupport();
    virtual ~LODSupport();

    enum UIDensity
    {
        Cramped   = 0x1,
        Compact   = 0x2,
        Maximized = 0x4
    };

    enum LODLevel
    {
        All     = 0x512,
        Medium  = 0x256,
        Minimal = 0x128,
        Zero    = 0x64
    };

    struct LODThreshold {
        QSize ALL;
        QSize Medium;
        QSize Minimal;
        QSize Zero;

        LODLevel    getLODLevel(QSize target);
        static bool sizeFits(QSize source, QSize target);
    };


    virtual void setLOD(LODLevel newLOD);
    virtual void setUIDensity(UIDensity newUIDensity);
    virtual void updateUIFromLOD();
    virtual void fitToSize(QSize widgetSize);
    virtual void updateToNewLOD(LODLevel newLOD);
    virtual void updateToNewDensity(UIDensity newDensity);
    virtual void updateTresholds();
    static bool  lodFits(LODLevel source, LODLevel target);

    void decreaseLOD();
    void increaseLOD();
    void increaseDensity();
    void decreaseDensity();

    LODThreshold getLodThreshold() const;
    void         setLodThreshold(const LODThreshold& value);

  protected:
    virtual void  set_cramped_minimal();
    virtual void  set_cramped_all();
    virtual void  set_maximized_all();
    virtual void  clear_all_layouts();
    virtual QSize getTreshold(LODLevel lodLevel);


    LODThreshold lodThreshold;
    LODLevel     currentLOD;
    UIDensity    currentDensity;
};


Q_DECLARE_METATYPE(LODSupport::UIDensity)
Q_DECLARE_METATYPE(LODSupport::LODLevel)

#endif // LOGSUPPORT_HPP
