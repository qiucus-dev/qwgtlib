HEADERS *= \
    $$PWD/waitingspinner.h \
    $$PWD/multiselect.hpp \
    $$PWD/twowaybutton.hpp \
    $$PWD/uiseparator.hpp \
    $$PWD/stringprompt.hpp

SOURCES *= \
    $$PWD/multiselect_ui.cpp \
    $$PWD/multiselect.cpp \
    $$PWD/twowaybutton.cpp \
    $$PWD/uiseparator.cpp \
    $$PWD/waitingspinner.cpp \
    $$PWD/stringprompt.cpp

INCLUDEPATH *= $$PWD
