#include "stringprompt.hpp"
#include <QHBoxLayout>
#include <QLineEdit>
#include <QTimer>
#include <hsupport/misc.hpp>

namespace wgt {
StringPrompt::StringPrompt(QWidget* parent)
    : QDialog(parent, Qt::FramelessWindowHint | Qt::Popup)
    , lineEdit(new QLineEdit(this))
    , layout(new QHBoxLayout(this))
//  ,
{
    setLayout(layout);
    layout->addWidget(lineEdit);
    setContentsMargins(spt::zeroMargins());
    layout->setContentsMargins(spt::zeroMargins());

    connect(lineEdit, &QLineEdit::returnPressed, [&]() {
        emit this->stringSelected(lineEdit->text());
        close();
    });
}

void StringPrompt::grabFocus() {
    QTimer::singleShot(0, lineEdit, SLOT(setFocus()));
}

void StringPrompt::selectAllUntil(QString delimiter) {
    lineEdit->setSelection(0, lineEdit->text().indexOf(delimiter));
}

void StringPrompt::setText(QString& text) {
    lineEdit->setText(text);
}

} // namespace wgt
